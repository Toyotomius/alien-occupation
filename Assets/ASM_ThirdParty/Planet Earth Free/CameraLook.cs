﻿using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class CameraLook : MonoBehaviour
{
    public Transform target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    public bool cameraLook;
    public Quaternion rotation;
    public Vector3 position;
    private Vector3 negDistance;

    private Rigidbody rigidbody;

    float x = 0.0f;
    float y = 0.0f;

    // Use this for initialization
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        rigidbody = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (rigidbody != null)
        {
            rigidbody.freezeRotation = true;
        }
    }
    void Update()
    {
        if(Input.GetMouseButton(2) || Input.GetKey(KeyCode.LeftAlt))
        {
            cameraLook = true;
        }
        else { cameraLook = false; }
    }

    void LateUpdate()
    {
        
        
        if(target && cameraLook)
        {
            x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
            y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            rotation = Quaternion.Euler(y, x, 0);
        }
        if (target && !EventSystem.current.IsPointerOverGameObject())
        {


            distance = Mathf.Clamp(distance - (Input.GetAxis("Mouse ScrollWheel") * 5), distanceMin, distanceMax);

            //RaycastHit hit;
            //if (Physics.Linecast(target.position, transform.position, out hit))
            //{
            //    distance -= hit.distance;
            //}
            negDistance = new Vector3(0.0f, 0.0f, -distance);
            
            var step1 = rotation * negDistance;
            var step2 = step1 + target.position;
            position = (rotation * negDistance) + target.position;

            transform.rotation = rotation;
            transform.position = position;
        }
        
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

}
