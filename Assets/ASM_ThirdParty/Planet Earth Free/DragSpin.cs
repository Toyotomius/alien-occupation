﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragSpin : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Sensitivity adjustment for manual spin. Default: 0.0002F")]
    public float sensitivity = 0.0002F;

    private Vector3 mouseDelta = Vector3.zero;
    private Vector3 mousePrevPos = Vector3.zero;

    public void OnMouseExit()
    {
        var dragScript = transform.GetComponent<DragSpin>();

        if (!Input.GetMouseButton(0)) 
        {
            if (dragScript.enabled == true) { dragScript.enabled = false; }
        }
        
    }

    public void OnMouseOver()
    {
        var dragScript = transform.GetComponent<DragSpin>();
        if (dragScript.enabled != true) { dragScript.enabled = true; }
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            var spin = this.GetComponent<SpinFree>();
            spin.spin = false;
            mouseDelta = Input.mousePosition - mousePrevPos;
            transform.Rotate(-Vector3.up, Vector3.Dot(mousePrevPos, mouseDelta) * sensitivity);
            
        }
        if (Input.GetMouseButtonUp(0)) { Physics.SyncTransforms(); OnMouseExit(); }
        mousePrevPos = Input.mousePosition;
    }
}
