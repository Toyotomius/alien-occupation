using AlienOccupation.Assets.Resources.Global;
using NUnit.Framework;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests.Assets.ASM_Tests
{
    public class GlobalVariablesTest
    {
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use `yield return
        // null;` to skip a frame.
        [UnityTest]
        public IEnumerator DateAndTime_IncrementsDuringPlay()
        {
            DateTime startDateTime = GameData.GDInstance.GlobalVariables.Date;

            yield return new WaitForSeconds(0.1F);

            DateTime newDateTime = GameData.GDInstance.GlobalVariables.Date;

            Assert.Less(startDateTime, newDateTime);
        }

        [UnityTest]
        public IEnumerator FastButton_ChangesTimeScale()
        {
            GameObject.Find("Fast").GetComponent<Button>().onClick.Invoke();

            yield return new WaitForSeconds(0.1F);

            Assert.AreEqual(2F, Time.timeScale);
        }

        [UnityTest]
        public IEnumerator FastestButton_ChangesTimeScale()
        {
            GameObject.Find("Fastest").GetComponent<Button>().onClick.Invoke();

            yield return new WaitForSeconds(0.1F);

            Assert.AreEqual(4F, Time.timeScale);
        }

        [UnityTest]
        public IEnumerator PauseButton_ChangesTimeScale()
        {
            GameObject.Find("Pause").GetComponent<Button>().onClick.Invoke();

            Assert.AreEqual(0F, Time.timeScale);

            yield return null;
        }

        [UnityTest]
        public IEnumerator PlayButton_ChangesTimeScale()
        {
            Time.timeScale = 0F;
            GameObject.Find("Play").GetComponent<Button>().onClick.Invoke();

            yield return new WaitForSeconds(0.1F);

            Assert.AreEqual(1, Time.timeScale);
        }

        [OneTimeSetUp]
        public void Setup()
        {
            // Load required Scene
            SceneManager.LoadScene(Constants.StrategicLayerIndex, LoadSceneMode.Single);

            // Set timescale to allow changes to occur.
            Time.timeScale = 1F;
        }
    }
}
