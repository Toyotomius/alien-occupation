﻿using AlienOccupation.Assets.Resources.Global;
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests.Assets.ASM_Tests
{
    public class GameStartMenuTests
    {
        private GameObject GameLoadCanvas;

        [UnityTest]
        public IEnumerator NewGame_LoadsStrategicLayerScene()
        {
            GameObject.Find("MainMenuNewGame").GetComponent<Button>().onClick.Invoke();

            yield return new WaitForFixedUpdate();

            Assert.AreEqual(Constants.StrategicLayerIndex, SceneManager.GetActiveScene().buildIndex);
        }
        [UnityTest]
        public IEnumerator NewGame_SetsNewGameFlag_True()
        {
            GameObject.Find("MainMenuNewGame").GetComponent<Button>().onClick.Invoke();

            yield return new WaitForFixedUpdate();

            Assert.IsTrue(GameData.GDInstance.GlobalVariables.IsNewGame);
        }

        [OneTimeSetUp]
        public void Setup()
        {
            GameLoadCanvas = GameObject.Find("GameLoadCanvas");
            SceneManager.LoadScene(Constants.GameStartMenuIndex, LoadSceneMode.Single);
            Time.timeScale = 1F;
        }
    }
}
