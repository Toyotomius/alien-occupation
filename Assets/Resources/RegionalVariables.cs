using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer
{
    public class RegionalVariables : MonoBehaviour
    {
        public string SlotForBuild;
        public string SubRegion;
    }
}
