﻿using AlienOccupation.Assets.Resources.Global.Extensions;
using System;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    /// <summary>
    /// Factory for retrieving and instantiating all buildings that use the IBuilding interface.
    /// Also maintains a secondary dictionary to keep track of all currently active buildings.
    /// </summary>
    public static class BuildingFactory
    {
        public static Dictionary<IBuilding, int> ActiveBuildings = new Dictionary<IBuilding, int>();

        private static Dictionary<string, Type> BuildingDict = new Dictionary<string, Type>();

        // Use to track researched buildings.
        private static Dictionary<string, IBuilding> ResearchedBuildings = new Dictionary<string, IBuilding>();

        static BuildingFactory()
        {
            var type = typeof(IBuilding);
            BuildingDict = type.CreateTypeDictionary(BuildingDict);
            AddDefaultBuildings();
        }

        public static void AddResearchedBuilding(IBuilding building)
        {
            if (!ResearchedBuildings.ContainsKey(building.Name))
            {
                ResearchedBuildings.Add(building.Name, building);
            }
            else { UnityEngine.Debug.LogError($"{building.Name} is already in the researched building dictionary"); }
        }
        public static void AddResearchedBuilding(string name)
        {
            if (!ResearchedBuildings.ContainsKey(name))
            {
                ResearchedBuildings.Add(name, GetBuilding(name));
            }
            else { UnityEngine.Debug.LogError($"{name} is already in the researched building dictionary"); }
        }

        public static void ClearResearchedBuildings()
        {
            ResearchedBuildings.Clear();
            AddDefaultBuildings();
        }

        public static IBuilding GetBuilding(string name)
        {
            if (!BuildingDict.ContainsKey(name))
            {
                UnityEngine.Debug.LogError($"{name} does not exist in BuildingDict.");
                return new ErrorBuilding();
            }
            return (IBuilding)Activator.CreateInstance(BuildingDict[name]);
        }

        public static Dictionary<string, IBuilding> GetResearchedBuildings()
        {
            return ResearchedBuildings;
        }

        public static void UpdateActiveBuildings(IBuilding building, bool disableBuilding = false)
        {
            if (!disableBuilding)
            {
                if (ActiveBuildings.ContainsKey(building))
                {
                    ActiveBuildings[building] += 1;
                }
                else
                {
                    ActiveBuildings.Add(building, 1);
                }
            }
            else
            {
                if (!ActiveBuildings.ContainsKey(building))
                {
                    throw new ArgumentException("You have attempted to remove a building that isn't built");
                }
                else
                {
                    ActiveBuildings[building] -= 1;
                }
            }
        }

        private static void AddDefaultBuildings()
        {
            foreach (var building in BuildingDict)
            {
                var actBuild = (IBuilding)Activator.CreateInstance(BuildingDict[building.Value.Name]);
                if (actBuild.IsResearched && !ResearchedBuildings.ContainsKey(actBuild.Name))
                {
                    ResearchedBuildings.Add(actBuild.Name, actBuild);
                }
            }
        }
    }
}
