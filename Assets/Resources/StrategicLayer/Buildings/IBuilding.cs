﻿using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using Newtonsoft.Json;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    public interface IBuilding
    {
        [JsonIgnore]
        Sprite BuildingSprite { get; }

        int BuildingTier { get; }

        string BuildingType { get; }

        int BuildTime { get; set; }

        int Cost { get; set; }

        int CurrentHitpoints { get; set; }

        string Description { get; set; }

        string DisplayName { get; }

        bool IsResearched { get; set; }

        int MaxHitPoints { get; set; }

        string Name { get; set; }

        BuildingState State { get; set; }

        ISubRegion SubRegionLocation { get; set; }

        void ApplyDamage();

        void ApplyEffects();

        void RemoveEffects();
    }
}
