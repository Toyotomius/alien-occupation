﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    internal class ErrorBuilding : IBuilding
    {
        public Sprite BuildingSprite { get; set; } = Constants.EmptyBuilingSprite;

        public int BuildingTier { get; set; }

        public string BuildingType { get; set; }

        public int BuildTime { get; set; }

        public int Cost { get; set; }

        public int CurrentHitpoints { get; set; } = 0;

        public string Description { get; set; } = "If you see this then a building didn't exist in the building dictionary";

        public string DisplayName { get; } = "Error Plot";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 0;

        public string Name { get; set; } = "ErrorBuilding";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
        }

        public void ApplyEffects()
        {
        }

        public void RemoveEffects()
        {
        }
    }
}
