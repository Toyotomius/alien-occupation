﻿using System.Collections;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    public interface IBuildingManager
    {
        IBuilding Add(IBuilding building);
        void ApplyDamage(IBuilding building, int amount);
        IBuilding CancelBuilding(IBuilding building);
        IBuilding Remove(IBuilding building);
        IEnumerator StartRepairCoroutine(IBuilding building);
    }
}