﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    public enum BuildingState
    {
        None,
        Started,
        Halfway,
        Built,
        Damaged
    }
}
