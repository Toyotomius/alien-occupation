﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    internal class EmptyBuilding : IBuilding
    {
        public Sprite BuildingSprite { get; set; } = Constants.EmptyBuilingSprite;

        public int BuildingTier { get; set; } = -1;

        public string BuildingType { get; set; }

        public int BuildTime { get; set; }

        public int Cost { get; set; }

        public int CurrentHitpoints { get; set; } = 0;

        public string Description { get; set; } = "An empty building space";

        public string DisplayName { get; } = "Empty Plot";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 0;

        public string Name { get; set; } = "EmptyBuilding";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
        }

        public void ApplyEffects()
        {
        }

        public void RemoveEffects()
        {
        }
    }
}
