﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Supression
{
    public class StrongMilitaryTower : IBuilding
    {
        private double CurrentRevoltRiskReduction = 0;

        private double DeltaRiskReduction = 0.8D;

        private bool isSubscribed = false;

        private int MaxReduction = 20;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 4;

        public string BuildingType { get; } = "MindControl";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Placeholder Military Tower Description";

        public string DisplayName { get; } = "Strong Military Tower";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "StrongMilitaryTower";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new System.NotImplementedException();
        }

        public void ApplyEffects()
        {
            if (!isSubscribed)
            {
                var dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();

                dateAndTime.WeekChange += UpdateRevoltRisk;
                isSubscribed = true;
            }
        }

        public void RemoveEffects()
        {
            if (isSubscribed)
            {
                var dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();
                dateAndTime.WeekChange -= UpdateRevoltRisk;
            }
            try
            {
                SubRegionLocation.SubRegionStats["RevoltRisk"] += CurrentRevoltRiskReduction;
            }
            catch (KeyNotFoundException e)
            {
                UnityEngine.Debug.LogError($"{e.Source} : SubRegion Stats 'RevoltRisk' not found");
            }
            CurrentRevoltRiskReduction = 0;
        }

        private void UpdateRevoltRisk(object sender, DateAndTime.TimeFrame arg)
        {
            if (arg == DateAndTime.TimeFrame.Weekly)
            {
                if (CurrentRevoltRiskReduction < MaxReduction)
                {
                    try
                    {
                        SubRegionLocation.SubRegionStats["RevoltRisk"] -= DeltaRiskReduction;
                        CurrentRevoltRiskReduction += DeltaRiskReduction;
                    }
                    catch (KeyNotFoundException e)
                    {
                        UnityEngine.Debug.LogError($"{e.Source} : SubRegion Stats 'RevoltRisk' not found");
                    }
                }
            }
        }
    }
}
