﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.MindControl
{
    public class BasicSlaveCenter : IBuilding
    {
        private int PopDecrease = 75;

        private int SlavesIncrease = 60;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 0;

        public string BuildingType { get; } = "MindControl";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Placeholder Slave Center Description";

        public string DisplayName { get; } = "Basic Slave Center";

        public bool IsResearched { get; set; } = true;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "BasicSlaveCenter";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new System.NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Slaves"].RateOfChange += SlavesIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Slaves' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange -= PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Slaves"].RateOfChange -= SlavesIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Slaves' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange += PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
        }
    }
}
