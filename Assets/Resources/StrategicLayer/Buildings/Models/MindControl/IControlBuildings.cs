﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Control
{
    public interface IControlBuildings
    {
        void ApplyEffects();
    }
}
