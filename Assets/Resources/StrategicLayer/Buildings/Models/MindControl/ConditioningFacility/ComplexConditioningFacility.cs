﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.MindControl
{
    public class ComplexConditioningFacility : IBuilding
    {
        private int DronesDecrease = 9;

        private int ManpowerIncrease = 30;

        private int PopDecrease = 35;

        private int WeaponDecrease = 30;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 2;

        public string BuildingType { get; } = "MindControl";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                            "Placeholder Conditioning Facility Description";

        public string DisplayName { get; } = "Complex Conditioning Facility";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "ComplexConditioningFacility";

        public BuildingState State { get; set; }

        //TODO: Create specialized manpower for Tier 2 & Tier 3 troops.
        //TODO: Make it possible to choose what types of troops a building focuses on.
        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new System.NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Manpower"].RateOfChange += ManpowerIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Manpower' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange -= PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Weapons"].RateOfChange -= WeaponDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Weapons' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Drones"].RateOfChange -= DronesDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Drones' does not exist. Check Resources.json");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Manpower"].RateOfChange -= ManpowerIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Manpower' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange += PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Weapons"].RateOfChange += WeaponDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Weapons' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Drones"].RateOfChange += DronesDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Drones' does not exist. Check Resources.json");
            }
        }
    }
}
