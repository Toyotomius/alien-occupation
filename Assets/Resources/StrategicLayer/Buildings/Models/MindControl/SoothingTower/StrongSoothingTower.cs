﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.MindControl
{
    public class StrongSoothingTower : IBuilding
    {
        private double CurrentHappinessBoost = 0;

        private double DeltaHappiness = 0.6D;

        private bool isSubscribed = false;

        private int MaxBoost = 15;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 3;

        public string BuildingType { get; } = "MindControl";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Placeholder Soothing Tower Description";

        public string DisplayName { get; } = "Strong Soothing Tower";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "StrongSoothingTower";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new System.NotImplementedException();
        }

        public void ApplyEffects()
        {
            if (!isSubscribed)
            {
                var dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();

                dateAndTime.WeekChange += UpdateHappiness;
                isSubscribed = true;
            }
        }

        public void RemoveEffects()
        {
            if (isSubscribed)
            {
                var dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();
                dateAndTime.WeekChange -= UpdateHappiness;
            }
            try
            {
                SubRegionLocation.SubRegionStats["Happiness"] -= CurrentHappinessBoost;
            }
            catch (KeyNotFoundException e)
            {
                UnityEngine.Debug.LogError($"{e.Source} : SubRegion Stats 'Happiness' not found");
            }

            CurrentHappinessBoost = 0;
        }

        private void UpdateHappiness(object sender, DateAndTime.TimeFrame arg)
        {
            if (arg == DateAndTime.TimeFrame.Weekly)
            {
                if (CurrentHappinessBoost < MaxBoost)
                {
                    try
                    {
                        SubRegionLocation.SubRegionStats["Happiness"] += DeltaHappiness;
                        CurrentHappinessBoost += DeltaHappiness;
                    }
                    catch (KeyNotFoundException e)
                    {
                        UnityEngine.Debug.LogError($"{e.Source} : SubRegion Stats 'Happiness' not found");
                    }
                }
            }
        }
    }
}
