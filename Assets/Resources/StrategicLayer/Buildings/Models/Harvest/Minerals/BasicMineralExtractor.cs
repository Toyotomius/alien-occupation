﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Harvest
{
    public class BasicMineralExtractor : IBuilding
    {
        private int MaterialsDecrease = 5;

        private int MineralIncrease = 20;

        private int PopDecrease = 10;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 0;

        public string BuildingType { get; } = "Harvest";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Placeholder Mineral Extractor Description";

        public string DisplayName { get; } = "Basic Extractor";

        public bool IsResearched { get; set; } = true;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "BasicMineralExtractor";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Minerals"].RateOfChange += MineralIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Minerals' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange -= PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Materials"].RateOfChange -= MaterialsDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Materials' does not exist. Check Resources.json");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Minerals"].RateOfChange -= MineralIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Minerals' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange += PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Materials"].RateOfChange += MaterialsDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Materials' does not exist. Check Resources.json");
            }
        }
    }
}
