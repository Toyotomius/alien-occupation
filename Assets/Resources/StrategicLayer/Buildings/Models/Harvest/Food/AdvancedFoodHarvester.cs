﻿using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System;
using System.Collections.Generic;
using UnityEngine;
using AlienOccupation.Assets.Resources.Global;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Harvest
{
    public class AdvancedFoodHarvester : IBuilding
    {
        private int FoodIncrease = 40;

        private int PopDecrease = 1200;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 3;

        public string BuildingType { get; } = "Harvest";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Production of 'Soylent Green' increases significantly at a much higher efficiency than previous models in" +
            "this Advanced Harvester";

        public string DisplayName { get; } = "Advanced Harvester";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "AdvancedFoodHarvester";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Food"].RateOfChange += FoodIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Food' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange -= PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Food"].RateOfChange -= FoodIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Food' does not exist. Check Resources.json");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange += PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Population' does not exist. Check Resources.json");
            }
        }
    }
}
