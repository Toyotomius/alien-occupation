﻿using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System;
using System.Collections.Generic;
using UnityEngine;
using AlienOccupation.Assets.Resources.Global;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Harvest
{
    //TODO: Update error handling
    public class BasicFoodHarvester : IBuilding
    {
        private int FoodIncrease = 10;

        private int PopDecrease = 1000;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 0;

        public string BuildingType { get; } = "Harvest";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Inefficient food harvesting techniques make converting raw materials into what the humans" +
            "would call 'Soylent Green' slow and wasteful in this basic food harvester";

        public string DisplayName { get; } = "Basic Harvester";

        public bool IsResearched { get; set; } = true;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "BasicFoodHarvester";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Food"].RateOfChange += FoodIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound for Basic Food Harvester: Player Resource 'Food' does not exist." +
                    $" Check Resources.json\n{e.StackTrace}");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange -= PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound for Basic Food Harvester: Player Resource 'Population' does not exist." +
                    $" Check Resources.json\n{e.StackTrace}");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Food"].RateOfChange -= FoodIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound for Basic Food Harvester: Player Resource 'Food' does not exist." +
                    $" Check Resources.json\n{e.StackTrace}");
            }
            try
            {
                GameData.GDInstance.PlayerResources["Population"].RateOfChange += PopDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound for Basic Food Harvester: Player Resource 'Population' does not exist." +
                    $" Check Resources.json\n{e.StackTrace}");
            }
        }
    }
}
