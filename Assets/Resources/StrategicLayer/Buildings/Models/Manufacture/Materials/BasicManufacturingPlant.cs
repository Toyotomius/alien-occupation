﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Manufacture
{
    public class BasicManufacturingPlant : IBuilding
    {
        private int MaterialsIncrease = 20;

        private int MineralsDecrease = 30;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 0;

        public string BuildingType { get; } = "Manufacturing";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Placeholder Manufacturing Plant Description";

        public string DisplayName { get; } = "Basic Manufacturing Plant";

        public bool IsResearched { get; set; } = true;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "BasicManufacturingPlant";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Materials"].RateOfChange += MaterialsIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Materials' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Minerals"].RateOfChange -= MineralsDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Minerals' does not exist. Check Resources.json");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Materials"].RateOfChange -= MaterialsIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Materials' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Minerals"].RateOfChange += MineralsDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Minerals' does not exist. Check Resources.json");
            }
        }
    }
}
