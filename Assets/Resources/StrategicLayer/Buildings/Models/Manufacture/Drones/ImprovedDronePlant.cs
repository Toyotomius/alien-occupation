﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings.Manufacture
{
    public class ImprovedDronePlant : IBuilding
    {
        private int DroneIncrease = 20;

        private int MaterialsDecrease = 10;

        public Sprite BuildingSprite { get; } = Constants.HarvestBuildingTestSprite;

        public int BuildingTier { get; } = 1;

        public string BuildingType { get; } = "Manufacturing";

        public int BuildTime { get; set; } = (int)(10 * GameData.GDInstance.GlobalVariables.BuildTimeModifier);

        public int Cost { get; set; } = (int)(100 * GameData.GDInstance.GlobalVariables.BuildCostModifier);

        public int CurrentHitpoints { get; set; } = 100;

        public string Description { get; set; } =
                    "Placeholder Drone Plant Description";

        public string DisplayName { get; } = "Improved Drone Plant";

        public bool IsResearched { get; set; } = false;

        public int MaxHitPoints { get; set; } = 100;

        public string Name { get; set; } = "ImprovedDronePlant";

        public BuildingState State { get; set; }

        public ISubRegion SubRegionLocation { get; set; }

        public void ApplyDamage()
        {
            throw new NotImplementedException();
        }

        public void ApplyEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Drones"].RateOfChange += DroneIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Drones' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Materials"].RateOfChange -= MaterialsDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Materials' does not exist. Check Resources.json");
            }
        }

        public void RemoveEffects()
        {
            try
            {
                GameData.GDInstance.PlayerResources["Drones"].RateOfChange -= DroneIncrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Drones' does not exist. Check Resources.json");
            }

            try
            {
                GameData.GDInstance.PlayerResources["Materials"].RateOfChange += MaterialsDecrease;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogError($"KeyNotFound at {e.Source}: Player Resource 'Materials' does not exist. Check Resources.json");
            }
        }
    }
}
