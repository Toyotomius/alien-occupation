﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using System.Collections;
using UnityEngine;
using static AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.DateAndTime;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Buildings
{
    /// <summary> Manager class for registering changes to building slots. </summary>
    public class BuildingManager : IBuildingManager
    {
        //TODO: Maybe a better solution than this to prevent same day ticks
        private int TimePassed = -1;

        /// <summary> Returns the requested building after starting the construction coroutine. </summary>
        /// <param name="buildingName"> Name of the building Type requested </param>
        /// <returns> Instantiated IBuilding type </returns>
        public IBuilding Add(IBuilding building)
        {
            // TODO: Add building to requested slot. Remember to check if slot is currently empty.
            //TODO: Do I add it as active here and check building state later? Or adjust removal to account for inactive but slotted buildings.
            //TODO: Make sure to add a confirmation popup if someone clicks to change building while paused. Refund amount if switched.

            StaticStartCoroutine.StartCoroutine(StartBuildingCoroutine(building));
            return building;
        }

        /// <summary> Applies any required damage to specified building. </summary>
        /// <param name="building"> Building to apply damage to </param>
        /// <param name="amount">   Amount of damage to apply </param>
        public void ApplyDamage(IBuilding building, int amount)
        {
            if (building.State != BuildingState.None)
            {
                if (building.CurrentHitpoints >= amount)
                {
                    building.CurrentHitpoints -= amount;
                }
                else { building.CurrentHitpoints = 0; }

                building.State = BuildingState.Damaged;
                UnityEngine.Debug.Log($"{building.DisplayName} damaged {amount} on {GameData.GDInstance.GlobalVariables.Date}");
                building.RemoveEffects();
                BuildingFactory.UpdateActiveBuildings(building, true);

                StaticStartCoroutine.StartCoroutine(StartRepairCoroutine(building));
            }
        }

        public IBuilding CancelBuilding(IBuilding building)
        {
            return new EmptyBuilding();
        }

        /// <summary>
        /// Removes existing building from specified slot and returns an empty building object.
        /// </summary>
        /// <param name="building"> Building slot to remove. Should be the same as slot assignment </param>
        /// <returns> Empty building object to replace existing building in the slot </returns>
        public IBuilding Remove(IBuilding building)
        {
            BuildingFactory.UpdateActiveBuildings(building, true);
            var buildTime = building.BuildTime;
            building = new EmptyBuilding
            {
                BuildTime = buildTime
            };
            StaticStartCoroutine.StartCoroutine(RemoveBuildingCoroutine(building));
            return building;
        }

        /// <summary> Repair Coroutine for damaged buildings </summary>
        /// <param name="building"> Building to repair </param>
        /// <returns> </returns>
        public IEnumerator StartRepairCoroutine(IBuilding building)
        {
            //TODO: Offload this to Constant.cfg
            var daysPerRepairTick = 5;
            var amtRepairPerTick = 10;

            DateAndTime dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();
            dateAndTime.DayChange += BuildingTimer;

            var repairCost = (building.MaxHitPoints - building.CurrentHitpoints) / amtRepairPerTick;

            while (building.CurrentHitpoints < building.MaxHitPoints)
            {
                if (TimePassed > 0 && TimePassed % daysPerRepairTick == 0
                    && GameData.GDInstance.PlayerResources["Materials"].Count >= repairCost)
                {
                    building.CurrentHitpoints += amtRepairPerTick;
                    GameData.GDInstance.PlayerResources["Materials"].Count -= repairCost;

                    UnityEngine.Debug.Log($"{building.DisplayName} was repaired by {amtRepairPerTick} on " +
                        $"{GameData.GDInstance.GlobalVariables.Date}");
                }
                yield return null;
            }
            building.State = BuildingState.Built;
            BuildingFactory.UpdateActiveBuildings(building);
        }

        private void BuildingTimer(object sender, TimeFrame timeFrame)
        {
            //TODO: Maybe change this if building events added for during construction
            TimePassed++;
        }

        /// <summary> Coroutine that runs to deconstruct the building slot. </summary>
        /// <param name="building"> Empty building being restored </param>
        private IEnumerator RemoveBuildingCoroutine(IBuilding building)
        {
            building.RemoveEffects();
            building.State = BuildingState.Halfway;
            DateAndTime dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();
            dateAndTime.DayChange += BuildingTimer;
            while ((building.BuildTime / 3) - TimePassed > 0)
            {
                yield return null;
            }
            dateAndTime.DayChange -= BuildingTimer;
            TimePassed = 0;
            building.State = BuildingState.None;
        }

        /// <summary> Coroutine to begin construction of the requested building </summary>
        /// <param name="building"> Requested building </param>
        private IEnumerator StartBuildingCoroutine(IBuilding building)
        {
            building.State = BuildingState.Started;

            DateAndTime dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();

            dateAndTime.DayChange += BuildingTimer;
            while (building.BuildTime - TimePassed > 0)
            {
                yield return null;
            }
            dateAndTime.DayChange -= BuildingTimer;
            TimePassed = 0;
            building.State = BuildingState.Built;
            building.ApplyEffects();
            BuildingFactory.UpdateActiveBuildings(building, false);
        }
    }
}
