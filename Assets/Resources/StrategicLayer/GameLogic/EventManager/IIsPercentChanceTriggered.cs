﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager
{
    public interface IIsPercentChanceTriggered
    {
        bool IsTriggered(double percentChance);
    }
}