﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager
{
    public class IsPercentChanceTriggered : IIsPercentChanceTriggered
    {
        public bool IsTriggered(double percentChance)
        {
            var ranNum = UnityEngine.Random.Range(1, 100);
            if (ranNum <= percentChance)
            {
                return true;
            }
            return false;
        }
    }
}
