﻿using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents.Models;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameResources;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents
{
    public interface IPlayerNotification
    {
        void RandomEventNotifyPlayer(object sender, object EventDetails);
        
    }
}
