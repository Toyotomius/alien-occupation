﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents.Models;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using static AlienOccupation.Assets.Resources.Global.GameData;
using static AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.DateAndTime;

namespace AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents
{
    /// <summary>
    /// Enum to split events out to various levels of severity for applying event effects. Minor =
    /// Max. 2 Sub Regions in a single Region. Moderate = Min. of 2 Sub Regions PER Min 1 Region to
    /// Max of MaxRegion - 2 Major = World Wide. All the subs and regions. Massive world event.
    /// </summary>
    public enum GameEventSeverity
    {
        Minor,
        Moderate,
        Major
    }

    public class GameEventHandler : IGameEventHandler
    {
        private static JObject dailyEvents;

        private static JObject monthlyEvents;

        private static JObject weeklyEvents;

        private static JObject yearlyEvents;

        static GameEventHandler()
        {
        }

        // Event handler used to send fired event details to PlayerNotification subscriber.
        public event EventHandler<RandomEventDetails> NotifyPlayerOfEvent;

        public static IGameEventHandler GameEventHandlerInstance { get; } = new GameEventHandler();

        //TODO: Sanity checks.
        public JObject DailyEvents { get => dailyEvents; set => dailyEvents = value; }

        public JObject MonthlyEvents { get => monthlyEvents; set => monthlyEvents = value; }

        public JObject WeeklyEvents { get => weeklyEvents; set => weeklyEvents = value; }

        public JObject YearlyEvents { get => yearlyEvents; set => yearlyEvents = value; }

        /// <summary>
        /// Iterates through the event effects categories and sends the effects to the appropriate
        /// method after randomly determining the affected regions to apply the event effects. Used
        /// by each EventSelection class. ex://MajorEventSelection.cs
        /// </summary>
        /// <param name="eventParent"> Name of event in the Json file </param>
        /// <param name="severity">   
        /// Severity of the event passed to find appropriate number of regions
        /// </param>
        public void FindAffectedRegions(JToken eventParent, GameEventSeverity severity)
        {
            var applyEffects = Factory.CreateApplyEventAffects();
            foreach (JProperty category in eventParent)
            {
                switch (category.Name)
                {
                    case "SubRegionStats":
                        {
                            switch (severity)
                            {
                                case GameEventSeverity.Major:
                                    {
                                        applyEffects.ApplyMajorEffects(category, GDInstance.Regions.regionDictionary);
                                        break;
                                    }
                                case GameEventSeverity.Moderate:
                                    {
                                        var affectedRegions = new List<KeyValuePair<string, IRegion>>();
                                        var subRegionHash = new HashSet<ISubRegion>();
                                        for (var i = 0; i < UnityEngine.Random.Range(1, GDInstance.Regions.regionDictionary.Count - 2); i++)
                                        {
                                            var randRegion = GDInstance.Regions.regionDictionary
                                                    .ElementAt(UnityEngine.Random.Range(0, GDInstance.Regions.regionDictionary.Count));
                                            affectedRegions.Add(randRegion);

                                            for (var y = 0; y < UnityEngine.Random.Range(2,
                                                GDInstance.Regions.regionDictionary.ElementAt(i).Value.subRegions.Count); y++)
                                            {
                                                var randSub = randRegion.Value
                                                    .subRegions
                                                    .ElementAt(UnityEngine.Random.Range(0, randRegion.Value.subRegions.Count))
                                                    .Value;
                                                while (subRegionHash.Contains(randSub))
                                                {
                                                    if (subRegionHash.Contains(randRegion.Value.Central)
                                                        && subRegionHash.Contains(randRegion.Value.East)
                                                        && subRegionHash.Contains(randRegion.Value.West))
                                                    {
                                                        break;
                                                    }
                                                    randSub = randRegion.Value
                                                    .subRegions
                                                    .ElementAt(UnityEngine.Random.Range(0, randRegion.Value.subRegions.Count))
                                                    .Value;
                                                }
                                                subRegionHash.Add(randSub);
                                            }
                                        }
                                        applyEffects.ApplyMinModEffects(category, subRegionHash);
                                        break;
                                    }
                                case GameEventSeverity.Minor:
                                    {
                                        var randRegion = GDInstance.Regions.regionDictionary
                                                    .ElementAt(UnityEngine.Random.Range(0, GDInstance.Regions.regionDictionary.Count));
                                        var affectedRegions = new Dictionary<string, IRegion>();

                                        affectedRegions[randRegion.Key] = randRegion.Value;
                                        var subRegionHash = new HashSet<ISubRegion>();
                                        for (var y = 0; y < UnityEngine.Random.Range(2,
                                                randRegion.Value.subRegions.Count); y++)
                                        {
                                            var randSub = randRegion.Value
                                                .subRegions
                                                .ElementAt(UnityEngine.Random.Range(0, randRegion.Value.subRegions.Count))
                                                .Value;
                                            subRegionHash.Add(randSub);
                                        }
                                        applyEffects.ApplyMinModEffects(category, subRegionHash);
                                        OnEventEffectsApply(this, new RandomEventDetails
                                        {
                                            AffectedRegions = affectedRegions,
                                            EventParent = eventParent,
                                            Description = eventParent["Description"].ToString()
                                        });

                                        break;
                                    }
                            }
                            break;
                        }
                    case "Resources":
                        {
                            applyEffects.ApplyResourceEffects(category);
                            if (!eventParent["SubRegionStats"].HasValues)
                            {
                                OnEventEffectsApply(this, new RandomEventDetails
                                {
                                    AffectedRegions = new Dictionary<string, IRegion>(),
                                    EventParent = eventParent
                                });
                                UnityEngine.Debug.Log($"Only Resource change was applied. {eventParent}");
                            }

                            break;
                        }
                }
            }
        }

        //TODO: Null checks.

        /// <summary>
        /// Loads all events from event json files located in:
        /// \Assets\Resources\StrategicLayer\EventManager\InGameEvents\ and assigns them to static
        /// JObject variables.
        /// </summary>
        public void Load()
        {
            var timeFrames = (TimeFrame[])Enum.GetValues(typeof(TimeFrame));

            foreach (var timeFrame in timeFrames)
            {
                using (var sr = new StreamReader($@"{Application.streamingAssetsPath}\{timeFrame}Events.json"))
                {
                    switch (timeFrame)
                    {
                        case (TimeFrame.Daily):
                            {
                                DailyEvents = JObject.Parse(sr.ReadToEnd());
                                break;
                            }
                        case (TimeFrame.Weekly):
                            {
                                WeeklyEvents = JObject.Parse(sr.ReadToEnd());
                                break;
                            }
                        case (TimeFrame.Monthly):
                            {
                                MonthlyEvents = JObject.Parse(sr.ReadToEnd());
                                break;
                            }
                        case (TimeFrame.Yearly):
                            {
                                YearlyEvents = JObject.Parse(sr.ReadToEnd());
                                break;
                            }
                    }
                }
            }
        }

        protected virtual void OnEventEffectsApply(object sender, RandomEventDetails e)
        {
            EventHandler<RandomEventDetails> handler = NotifyPlayerOfEvent;
            handler?.Invoke(this, e);
        }
    }
}

//TODO: Make Yearly and Monthly events not empty. Seems like a good idea.
