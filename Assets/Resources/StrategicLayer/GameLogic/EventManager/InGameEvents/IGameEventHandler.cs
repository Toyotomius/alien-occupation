﻿using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents.Models;
using Newtonsoft.Json.Linq;
using System;

namespace AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents
{
    public interface IGameEventHandler
    {
        event EventHandler<RandomEventDetails> NotifyPlayerOfEvent;

        JObject DailyEvents { get; set; }

        JObject MonthlyEvents { get; set; }

        JObject WeeklyEvents { get; set; }

        JObject YearlyEvents { get; set; }

        void FindAffectedRegions(JToken eventParent, GameEventSeverity severity);

        void Load();
    }
}
