﻿using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents
{
    public interface IApplyEventEffects
    {

        
        // Moderate events
        void ApplyMinModEffects(JProperty category, HashSet<ISubRegion> affectedSubRegions);

        // Major events
        void ApplyMajorEffects(JProperty category, Dictionary<string, IRegion> affectedRegions);

        // Events that affect player resources
        void ApplyResourceEffects(JProperty category);
    }
}
