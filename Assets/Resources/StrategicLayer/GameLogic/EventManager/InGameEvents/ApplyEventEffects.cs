﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents
{
    /// <summary>
    /// Applies all event affects from the random in-game events listed in
    /// Daily/Weekly/Monthly/YearlyEvents.json \Assets\Resources\StrategicLayer\EventManager\InGameEvents\
    /// </summary>
    public class ApplyEventEffects : IApplyEventEffects
    {
        // Major events
        public void ApplyMajorEffects(JProperty category, Dictionary<string, IRegion> affectedRegions)
        {
            var tempSubRegionList = new List<ISubRegion>();
            foreach (var region in affectedRegions)
            {
                tempSubRegionList.Add(region.Value.West);
                tempSubRegionList.Add(region.Value.East);
                tempSubRegionList.Add(region.Value.Central);
            }
            foreach (var itm in tempSubRegionList)
            {
                foreach (JProperty effect in category.Value)
                {
                    itm[effect.Name] += (double)effect.Value;
                }
            }
        }

        // Minor & Moderate events
        public void ApplyMinModEffects(JProperty category, HashSet<ISubRegion> affectedSubRegions)
        {
            foreach (JProperty effect in category.Value)
            {
                foreach (var sub in affectedSubRegions)
                {
                    sub[effect.Name] += (double)effect.Value;
                }
            }
        }

        // Events that affect player resources
        public void ApplyResourceEffects(JProperty category)
        {
            foreach (JProperty effect in category.Value)
            {
                GameData.GDInstance.PlayerResources[effect.Name].Count += (int)effect.Value;
            }
        }
    }
}
