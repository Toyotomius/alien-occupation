﻿using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents.Models
{
    /// <summary>
    /// Simple model for any other events that be used with the PlayerNotification class
    /// </summary>
    public class GenericEventDetails
    {
        public string Description { get; set; }

        public string RegionName { get; set; }
    }

    /// <summary>
    /// Model used for events pushed to the PlayerNotification subscriber from in game random events
    /// via the event json files.
    /// </summary>
    public class RandomEventDetails
    {
        public Dictionary<string, IRegion> AffectedRegions { get; set; }

        public string Description { get; set; }

        public JToken EventParent { get; set; }
    }

    /// <summary> Model used for revolt notifications </summary>
    public class RevoltEventDetails
    {
        public Dictionary<IBuilding, double> BuildingDict { get; set; }

        public string RegionName { get; set; }

        public Dictionary<string, double> ResourceDict { get; set; }

        public string SubregionName { get; set; }

        public Dictionary<string, double> SubregionStatDict { get; set; }
    }
}
