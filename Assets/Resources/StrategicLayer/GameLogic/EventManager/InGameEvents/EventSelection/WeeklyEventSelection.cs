﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents;
using Newtonsoft.Json.Linq;
using static AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.DateAndTime;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents
{
    /// <summary>
    /// Grabs events of appropriate severity based on TimeFrame of event then checks if event has
    /// randomly fired. If it has, sends to EventHandler to find affected regions who then sends it
    /// to ApplyEventEffects.
    /// </summary>
    public class WeeklyEventSelection : IEventSelection
    {
        /// <summary> Subsciber to DateAndTime event publisher for Weekly time change. </summary>
        /// <param name="sender"> DateAndTime OnTimeChange </param>
        /// <param name="e">      EventArg <TimeFrame> enum </TimeFrame> </param>
        public void SelectEvent(object sender, TimeFrame e)
        {
            var eventHandler = Factory.CreateGameEventHandler();
            var isTriggered = Factory.CreateIsPercentChanceTriggered();

            foreach (JToken weeklyEvent in eventHandler.WeeklyEvents["Minor"])
            {
                if (isTriggered.IsTriggered((double)weeklyEvent["PercentChance"]))
                {
                    eventHandler.FindAffectedRegions(weeklyEvent, GameEventSeverity.Minor);
                }
            }

            foreach (JToken weeklyEvent in eventHandler.WeeklyEvents["Moderate"])
            {
                if (isTriggered.IsTriggered((double)weeklyEvent["PercentChance"]))
                {
                    eventHandler.FindAffectedRegions(weeklyEvent, GameEventSeverity.Moderate);
                }
            }

            foreach (JToken weeklyEvent in eventHandler.WeeklyEvents["Major"])
            {
                if (isTriggered.IsTriggered((double)weeklyEvent["PercentChance"]))
                {
                    eventHandler.FindAffectedRegions(weeklyEvent, GameEventSeverity.Major);
                }
            }
        }
    }
}
