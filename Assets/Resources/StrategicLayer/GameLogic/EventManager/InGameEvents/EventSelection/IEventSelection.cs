﻿using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents
{
    public interface IEventSelection
    {
        void SelectEvent(object sender, DateAndTime.TimeFrame e);
    }
}