﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents.Models;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents
{
    /// <summary>
    /// Instantiates prefabs for event flavor text and sets their parents appropriately to the event
    /// notification area. Each affected region has its own prefab to allow for easy click
    /// navigation to the region. Manages a queue of a set size to prevent content buffer from being
    /// too large.
    /// </summary>

    public class PlayerNotification : ScriptableObject, IPlayerNotification
    {
        private Transform EventFlavorParent;
        private Transform EventRegionParent;
        private Queue<GameObject> NotificationQueue = new Queue<GameObject>();

        static PlayerNotification()
        {
        }

        public static PlayerNotification PlayerNotificationInstance { get; }
            = (PlayerNotification)ScriptableObject.CreateInstance("PlayerNotification");

        /// <summary>
        /// Structures the gameobjects and descriptions for notifying the player of a random event occurring
        /// ex: revolt or random event triggered from the events in the {timeframe}Event jsons.
        /// </summary>
        /// <param name="sender">       event sender </param>
        /// <param name="EventDetails"> Class containing the event details, if any. </param>
        public void RandomEventNotifyPlayer(object sender, object EventDetails)
        {
            EventFlavorParent = Instantiate(Constants.EventFlavorParentPrefab).transform;
            EventRegionParent = EventFlavorParent.transform.Find("EventRegionParent");
            EventFlavorParent.transform.SetParent(GameObject.Find("EventContentParent").transform, false);

            switch (EventDetails)
            {
                // Random event from the timeframe based events with no region
                case RandomEventDetails e when e.AffectedRegions.Count == 0:
                    {
                        UnityEngine.Debug.Log("Notification Received. Only Resources.");
                        CreateNotification(string.Empty, e.Description);
                        break;
                    }

                // Random event from timeframe based events with region
                case RandomEventDetails e:
                    {
                        foreach (var region in e.AffectedRegions)
                        {
                            CreateNotification(region.Key, e.Description);
                        }

                        break;
                    }

                // Only building damage
                case RevoltEventDetails e when e.ResourceDict.Count == 0 && e.SubregionStatDict.Count == 0:
                    {
                        var bldStr = "";
                        
                        foreach (var bld in e.BuildingDict)
                        {
                            bldStr += $"{bld.Key.DisplayName} by {bld.Value}\n";

                        }
                        var desc = string.Format("{0} subregion of {1} had the following building damage:\n{2}.",
                            e.SubregionName, ParseRegionName.Parse(e.RegionName), bldStr);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // Building & additional subregion stat damage
                case RevoltEventDetails e when e.ResourceDict.Count == 0:
                    {
                        var bldStr = "";
                        foreach (var bld in e.BuildingDict)
                        {
                            bldStr += $"{bld.Key.DisplayName} by {bld.Value}\n";
                        }
                        var subStat = "";
                        foreach (var stat in e.SubregionStatDict)
                        {
                            subStat += $"{stat.Key}: {stat.Value}, ";
                        }
                        var desc = string.Format("{0} subregion of {1} had the following building damage:\n{2}" +
                            "In addition the subregion had {3}affected",
                            e.SubregionName, ParseRegionName.Parse(e.RegionName), bldStr, subStat);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // Building & Resource damage
                case RevoltEventDetails e when e.SubregionStatDict.Count == 0:
                    {
                        var bldStr = "";
                        foreach (var bld in e.BuildingDict)
                        {
                            bldStr += $"{bld.Key.DisplayName} by {bld.Value}\n";
                        }
                        var resStr = "";
                        foreach (var res in e.ResourceDict)
                        {
                            resStr += $"{res.Key} by {res.Value}, ";
                        }
                        var desc = string.Format("{0} subregion of {1} had the following building damage:\n{2}" +
                            "In addition these resources were affected: {3}",
                            e.SubregionName, ParseRegionName.Parse(e.RegionName), bldStr, resStr);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // Resource and subregion stat damage
                case RevoltEventDetails e when e.BuildingDict.Count == 0:
                    {
                        var subStat = "";
                        foreach (var stat in e.SubregionStatDict)
                        {
                            subStat += $"{stat.Key} by {stat.Value}, ";
                        }
                        var resStr = "";
                        foreach (var res in e.ResourceDict)
                        {
                            resStr += $"{res.Key} by {res.Value}, ";
                        }
                        var desc = string.Format("{0} subregion of {1} had {2} affected.\n" +
                           "In addition these resources were affected: {3}",
                           e.SubregionName, ParseRegionName.Parse(e.RegionName), subStat, resStr);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // Only Resource damage
                case RevoltEventDetails e when e.BuildingDict.Count == 0 && e.SubregionStatDict.Count == 0:
                    {
                        var resStr = "";
                        foreach (var res in e.ResourceDict)
                        {
                            resStr += $"{res.Value} of {res.Key}, ";
                        }
                        var desc = string.Format("{0} subregion of {1} had {2}stolen\n",
                           e.SubregionName, ParseRegionName.Parse(e.RegionName), resStr);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // Only Subregion stat damage
                case RevoltEventDetails e when e.BuildingDict.Count == 0 && e.ResourceDict.Count == 0:
                    {
                        var subStat = "";
                        foreach (var stat in e.SubregionStatDict)
                        {
                            subStat += $"{stat.Key} by {stat.Value}, ";
                        }
                        var desc = string.Format("{0} subregion of {1} had {2}reduced\n",
                           e.SubregionName, ParseRegionName.Parse(e.RegionName), subStat);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // All in
                case RevoltEventDetails e:
                    {
                        var bldStr = "";
                        foreach (var bld in e.BuildingDict)
                        {
                            bldStr += $"{bld.Key.DisplayName} by {bld.Value}\n";
                        }
                        var subStat = "";
                        foreach (var stat in e.SubregionStatDict)
                        {
                            subStat += $"{stat.Key} by {stat.Value}, ";
                        }
                        var resStr = "";
                        foreach (var res in e.ResourceDict)
                        {
                            resStr += $"{res.Key} by {res.Value}, ";
                        }
                        var desc = string.Format("{0} subregion of {1} had the following building damage:\n{2}" +
                            "In addition the subregion had {3} reductions\n" +
                            "Finally, these resources were stolen: {4}",
                            e.SubregionName, ParseRegionName.Parse(e.RegionName), bldStr, subStat, resStr);
                        CreateNotification(e.RegionName, desc);
                        break;
                    }

                // Generic details that contain just the affected region (if any) and event description
                case GenericEventDetails e:
                    {
                        CreateNotification(e.RegionName, e.Description);
                        break;
                    }
            }
        }

        /// <summary>
        /// Adds event to the notification queue, discards last notification if over 20. Displays
        /// new notification animation
        /// </summary>
        /// <param name="eventFlavorParent">
        /// Top level gameobject of the event being added to the queue
        /// </param>
        private void AddToNotificationQueue(GameObject eventFlavorParent)
        {
            NotificationQueue.Enqueue(eventFlavorParent);
            if (NotificationQueue.Count > 20)
            {
                GameObject discard = NotificationQueue.Dequeue();
                Destroy(discard);
            }

            /// If the event panel is not currently open, activates the notification animation for
            /// unread events.
            var isOpen = GameObject.Find("EventPanel").GetComponent<Animator>().GetBool("IsOpen");
            if (!isOpen)
            {
                GameObject.Find("EventExpandButton").GetComponent<Animator>().SetBool("HasUnread", true);
            }
        }

        /// <summary>
        /// Creates and arranges the last of the gameobjects required for the notification to be displayed
        /// </summary>
        /// <param name="RegionName">
        /// Affected region, parsed for any spaces or other changes needed for display
        /// </param>
        /// <param name="Desc">       Description created by RandomEventNotifyPlayer method </param>
        private void CreateNotification(string RegionName, string Desc)

        {
            var eventRegion = Instantiate(Constants.EventRegionPrefab);
            eventRegion.transform.SetParent(EventRegionParent, false);
            eventRegion.name = RegionName;
            eventRegion.GetComponent<Text>().text = ParseRegionName.Parse(RegionName);

            var eventFlavorText = EventFlavorParent.Find("EventFlavorText");
            eventFlavorText.GetComponent<Text>().text = Desc;
            eventFlavorText.transform.SetAsLastSibling();

            AddToNotificationQueue(EventFlavorParent.gameObject);
        }
    }

    // TODO: Check if AffectedRegions is null and create flavor text for when this is the case.
    // TODO: Make prettier.
    // TODO: Small rotation bug when clicking on regions
    // TODO: Time stamp?
}
