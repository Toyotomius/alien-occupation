﻿using AlienOccupation.Assets.Resources.Global;
using static AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.DateAndTime;

namespace AlienOccupation.Assets.Resources.StrategicLayer.EventManager
{
    /// <summary>
    /// Checks if revolts have fired in a subregion based on the sub's revolt risk value.
    /// </summary>
    public class RevoltCheck
    {
        public void TriggerCheck(object sender, TimeFrame e)
        {
            var isTriggered = Factory.CreateIsPercentChanceTriggered();

            if (e == TimeFrame.Weekly)
            {
                foreach (var region in GameData.GDInstance.Regions.regionDictionary)
                {
                    foreach (var sub in region.Value.subRegions)
                    {
                        if (isTriggered.IsTriggered(sub.Value["RevoltRisk"] / 5))
                        {
                            UnityEngine.Debug.Log($"Revolt Triggered on Weekly ({GameData.GDInstance.GlobalVariables.Date}) : {sub.Key} {region.Key}");
                        }
                    }
                }
            }
            if (e == TimeFrame.Monthly)
            {
                foreach (var region in GameData.GDInstance.Regions.regionDictionary)
                {
                    foreach (var sub in region.Value.subRegions)
                    {
                        if (isTriggered.IsTriggered(sub.Value["RevoltRisk"]))
                        {
                            UnityEngine.Debug.Log($"Revolt Triggered on monthly ({GameData.GDInstance.GlobalVariables.Date}) :  {sub.Key} {region.Key}");
                        }
                    }
                }
            }
        }
    }
}
