﻿using AlienOccupation.Assets.Resources.Global;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.EndGame.Failure
{
    public class FailScenarios
    {
        private readonly ILoadScene LoadScene = Factory.CreateLoadScene();
        //TODO: Switch to the variables from the Constants.cfg
        public void Defeated()
        {
            LoadScene.SceneLoad(2);
        }

        public void FailureResource()
        {
            LoadScene.SceneLoad(2);
        }

        public void TimeLimitReached()
        {
            LoadScene.SceneLoad(2);
        }
    }

    //TODO: Stats
}
