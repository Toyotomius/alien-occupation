﻿using Newtonsoft.Json;

namespace AlienOccupation.Assets.Resources.StrategicLayer.InGameResources
{
    public class Resources : IResource
    {
        public int Count { get; set; }
        public int Required { get; set; }
        [JsonIgnore]
        public string Name { get; set; }
        [JsonIgnore]
        public string TooltipText { get; set; }
        [JsonIgnore]
        public string SpriteLocation { get; set; }
        [JsonIgnore]
        public int RateOfChange { get; set; }
        //TODO: Add sanity checking. Make sure rate of change doesn't cause count to go below 0. Handle it if it does.
    }
}
