﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.InGameResources
{
    public interface IResource
    {
        int Count { get; set; }

        string Name { get; set; }

        int RateOfChange { get; set; }

        int Required { get; set; }

        string SpriteLocation { get; set; }

        string TooltipText { get; set; }
    }
}
