﻿using AlienOccupation.Assets.Resources.StrategicLayer.InGameResources;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AlienOccupation.Assets.Resources
{
    /// <summary> Class for all player resources </summary>
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    public class PlayerResources : IEnumerable<IResource>
    {
        private readonly dynamic DynResource;

        public PlayerResources()
        {
            string gameResourceJson;

            //TODO: Definitely fix this
            using (var sr = new StreamReader($@"{Application.streamingAssetsPath}\Resources.json"))
            {
                gameResourceJson = sr.ReadToEnd();
            }
            DynResource = JObject.Parse(gameResourceJson);

            Resources = new Dictionary<string, IResource>();

            foreach (dynamic resource in DynResource)
            {
                Resources.Add(resource.Name,
                    new StrategicLayer.InGameResources.Resources
                    {
                        Count = resource.Value.Amount,
                        Required = resource.Value.Required,
                        Name = resource.Name,
                        TooltipText = resource.Value.TooltipText,
                        SpriteLocation = resource.Value.SpriteLocation,
                        RateOfChange = resource.Value.RateOfChange
                    });
            }
        }

        [JsonProperty]
        public Dictionary<string, IResource> Resources { get; set; }

        public IResource this[string key]
        {
            get
            {
                return Resources[key];
            }
            set
            {
                Resources[key] = value;
            }
        }

        public IEnumerator<IResource> GetEnumerator()
        {
            return Resources.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void UpdateUnsavedProperties()
        {
            foreach (dynamic resource in DynResource)
            {
                if (!Resources.ContainsKey(resource.Name))
                {
                    Resources.Add(resource.Name,
                    new StrategicLayer.InGameResources.Resources
                    {
                        Count = resource.Value.Amount,
                        Required = resource.Value.Required,
                        Name = resource.Name,
                        TooltipText = resource.Value.TooltipText,
                        SpriteLocation = resource.Value.SpriteLocation,
                        RateOfChange = resource.Value.RateOfChange
                    });
                }
                else
                {
                    Resources.TryGetValue(resource.Name, out IResource temp);

                    temp.Name = resource.Name;
                    temp.TooltipText = resource.Value.TooltipText;
                    temp.SpriteLocation = resource.Value.SpriteLocation;

                    Resources[resource.Name] = temp;
                }
            }

            //TODO: Update & Cascade Tech
        }
    }
}

// TODO: Add population.
// TODO: Separate Quota resources into their own dictionary & interface specifically for changing quota related things?
// TODO: Jsonify resources? Are the models necessary? Might be more mod friendly? Load straight into dictionary with {name} && field mapping?
