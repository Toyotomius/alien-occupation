﻿using AlienOccupation.Assets.Resources.Global;
using System;
using static AlienOccupation.Assets.Resources.Global.GameData;
using static AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.DateAndTime;

namespace AlienOccupation.Assets.Resources.StrategicLayer.InGameResources
{
    /// <summary> Updates player resources in a given time frame </summary>
    public class ResourceUpdate
    {
        /// <summary> Updates resource count from passive income sources </summary>
        public void DailyUpdate(object sender, TimeFrame e)
        {
            foreach (var resource in GDInstance.PlayerResources)
            {
                resource.Count += (int)(resource.RateOfChange * GDInstance.GlobalVariables.Difficulty.ResourceRateModifier);
            }
        }

        /// <summary> Deducts the monthly quota requirement for resources and penalizes accordingly. </summary>
        public void MonthlyUpdate(object sender, TimeFrame e)
        {
            foreach (var resource in GDInstance.PlayerResources)
            {
                resource.Count -= (int)(resource.Required * GDInstance.GlobalVariables.Difficulty.QuotaRequirementModifier);
                if (resource.Count < 0)
                {
                    resource.Count = 0;
                    if (GDInstance.PlayerResources.Resources.ContainsKey("Favor"))
                    {
                        GDInstance.PlayerResources["Favor"].Count -= Constants.DefaultPenaltyResourceDeduction;
                        if (GDInstance.PlayerResources["Favor"].Count < -50)
                        {
                            // TODO: Initiate game failure chain.
                            throw new NotImplementedException();
                        }
                    }
                }
            }
        }

        public void YearlyAudit(object sender, TimeFrame e)
        {
            //TODO: Yearly audit chain
        }
    }
}
