﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.Global.Extensions;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents;
using AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts.Interfaces;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents.Models;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using static AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.DateAndTime;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts
{
    public class RevoltManager : IRevoltManager
    {
        private JArray AdditionalResourceDamage;
        private JArray AddtionalSubRegionDamage;
        private IBuilding[] BldArray;
        private Dictionary<IBuilding, double> BldDict = new Dictionary<IBuilding, double>();
        private KeyValuePair<string, IRegion> Region;
        private Dictionary<string, double> ResDict = new Dictionary<string, double>();
        private int RevoltRoll;
        private KeyValuePair<string, ISubRegion> SubRegion;
        private Dictionary<string, double> SubStatDict = new Dictionary<string, double>();

        /// <summary>
        /// Event handler method that cycles through each region and subregion to see if a revolt
        /// has occurred in that subregion.
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e">      TimeFrame to see severity of the revolt. </param>
        public void RevoltChecker(object sender, TimeFrame e)
        {
            var nameList = new List<string>();

            foreach (var region in GameData.GDInstance.Regions.regionDictionary)
            {
                nameList.Add(region.Key);
            }
            switch (e)
            {
                case TimeFrame.Weekly:
                    {
                        var counter = 0;

                        while (nameList.Count > 0)
                        {
                            var ranInd = UnityEngine.Random.Range(0, nameList.Count);
                            var name = nameList[ranInd];
                            nameList.RemoveAt(ranInd);

                            foreach (var subRegion in GameData.GDInstance.Regions.regionDictionary[name].subRegions)
                            {
                                if (!subRegion.Value.RecentlyRevolted && HasRevolted(subRegion, name, TimeFrame.Weekly))
                                {
                                    var sev = CalculateSeverity();
                                    ApplyEffects(sev);
                                }
                                subRegion.Value.RecentlyRevolted = false;
                            }
                            if (name == Region.Key)
                            {
                                counter++;
                            }

                            // Prevents too many regions from revolting weekly at once.
                            if (counter >= 2)
                                break;
                        }
                        break;
                    }
                case TimeFrame.Monthly:
                    {
                        while (nameList.Count > 0)
                        {
                            var ranInd = UnityEngine.Random.Range(0, nameList.Count);
                            var name = nameList[ranInd];
                            nameList.RemoveAt(ranInd);

                            foreach (var subRegion in GameData.GDInstance.Regions.regionDictionary[name].subRegions)
                            {
                                if (!subRegion.Value.RecentlyRevolted && HasRevolted(subRegion, name, TimeFrame.Weekly))
                                {
                                    var sev = CalculateSeverity();
                                    ApplyEffects(sev);
                                }
                                subRegion.Value.RecentlyRevolted = false;
                            }
                        }
                        break;
                    }
            }
        }

        /// <summary> Applies revolt effects to random building in subregion rioting </summary>
        /// <param name="severity"> Severity of the revolt to calculate the damage </param>
        private void AffectRandomBuilding(RevoltSeverity severity)
        {
            var randBldIndex = UnityEngine.Random.Range(0, BldArray.Length);
            var manager = Factory.CreateBuildingManager();

            var bld = BldArray[randBldIndex];

            // Base damage as a random % of building's max HP based on severity(inclusive)
            var bldDmgBase = bld.MaxHitPoints * Math.Round((double)(UnityEngine.Random.Range(0, (float)severity) / 100), 2);
            var dmgAmt = (int)(GameData.GDInstance.GlobalVariables.Difficulty.RevoltSeverity
                         * bldDmgBase * SubRegion.Value.SubRegionStats["RevoltSeverity"]);

            manager.ApplyDamage(bld, dmgAmt);

            // Tracking building & damage for notification
            if (BldDict.ContainsKey(bld))
            {
                BldDict[bld] += dmgAmt;
            }
            else
            {
                BldDict.Add(BldArray[randBldIndex], dmgAmt);
            }

            
        }

        /// <summary> Applies revolt effects to randomly selected player resource </summary>
        /// <param name="severity"> Severity of the revolt to calculate the damage </param>
        private void AffectRandomResource(RevoltSeverity severity)
        {
            var resDmg = GameData.GDInstance.GlobalVariables.Difficulty.RevoltSeverity
                         * (int)severity * 2 * SubRegion.Value.SubRegionStats["RevoltSeverity"];
            var randAddEffIndex = UnityEngine.Random.Range(0, AdditionalResourceDamage.Count);
            var resource = AdditionalResourceDamage[randAddEffIndex].ToString();

            GameData.GDInstance.PlayerResources[$"{resource}"].Count -= (int)resDmg;
            if (ResDict.ContainsKey(resource))
            {
                ResDict[resource] += resDmg;
            }
            else
            {
                ResDict.Add(resource, resDmg);
            }
        }

        /// <summary> Applies revolt effects to randomly selected subregion stat </summary>
        /// <param name="severity"> Severity of the revolt to calculate the damage </param>
        private void AffectRandomSubstat(RevoltSeverity severity)
        {
            var randAddEffIndex = UnityEngine.Random.Range(0, AddtionalSubRegionDamage.Count);
            var subRegionStat = AddtionalSubRegionDamage[randAddEffIndex].ToString();
            if (SubRegion.Value.SubRegionStats.ContainsKey(subRegionStat))
            {
                var dmgAmnt = (GameData.GDInstance.GlobalVariables.Difficulty.RevoltSeverity)
                    * (((int)severity / 10) * SubRegion.Value.SubRegionStats["RevoltSeverity"]);

                SubRegion.Value.SubRegionStats[subRegionStat] -= dmgAmnt;

                if (SubStatDict.ContainsKey(subRegionStat))
                {
                    SubStatDict[subRegionStat] += dmgAmnt;
                }
                else
                {
                    SubStatDict.Add(subRegionStat, dmgAmnt);
                }

            }
        }

        /// <summary> Applies the effect of the revolt based on severity </summary>
        /// <param name="severity"> enum to track how severe/damaging the revolt is </param>
        private void ApplyEffects(RevoltSeverity severity)
        {
            AddtionalSubRegionDamage = (JArray)Constants.RevoltSettings["AdditionalSubRegionDamage"];
            AdditionalResourceDamage = (JArray)Constants.RevoltSettings["AdditionalResourceDamage"];

            // Takes all buildings that have already been built and puts them in an array for random selection
            BldArray = SubRegion.Value.BuildingSlots.BuildingSlotDict
                                   .Where(x => x.Value.BuildingTier >= 0 &&
                                   (x.Value.State == BuildingState.Built || x.Value.State == BuildingState.Damaged))
                                   .Select(x => x.Value).ToArray();

            switch (severity)
            {
                case RevoltSeverity.Low:
                    {
                        // Applies damage via the BuildingManager to randomly selected building in subregion
                        if (BldArray.Length > 0)
                        {
                            AffectRandomBuilding(severity);
                        }

                        // Calculates chances for additional regional damage and applies it from the
                        // json array of potential stats
                        var rand = UnityEngine.Random.Range(0, 100);
                        if (rand < (int)severity || (BldArray.Length == 0 && rand < (int)severity * 2))
                        {
                            AffectRandomSubstat(severity);
                        }

                        // Calculates chance of additional resource damage. Forces resource damage
                        // if no other damage is present.
                        rand = UnityEngine.Random.Range(0, 100);
                        if (rand < (int)severity || (BldArray.Length == 0 && SubStatDict.Count == 0))
                        {
                            AffectRandomResource(severity);
                        }
                        
                        break;
                    }

                // Affects: 1 building damage instance possibly 2. 1 substat. randomly picks
                // resource damage twice
                case RevoltSeverity.Medium:
                    {
                        if (BldArray.Length > 0)
                        {
                            for (var i = 0; i < UnityEngine.Random.Range(0, 2); i++)
                                AffectRandomBuilding(severity);
                        }
                        var rand = UnityEngine.Random.Range(0, 100);
                        if (rand < (int)severity || (BldArray.Length == 0 && rand < (int)severity * 2))
                        {
                            AffectRandomSubstat(severity);
                        }
                        rand = UnityEngine.Random.Range(0, 100);
                        if (rand < (int)severity || (BldArray.Length == 0 && SubStatDict.Count == 0))
                        {
                            for (var i = 0; i < 2; i++)
                                AffectRandomResource(severity);
                        }

                        
                        break;
                    }

                // Affects: 2 buildings damage instance, possibly 3. 1, possibly 2 subregion stats,
                // 3 resource randoms
                case RevoltSeverity.High:
                    {
                        if (BldArray.Length > 0)
                        {
                            for (var i = 0; i < UnityEngine.Random.Range(1, 3); i++)
                                AffectRandomBuilding(severity);
                        }
                        var rand = UnityEngine.Random.Range(0, 100);
                        if (rand < (int)severity || (BldArray.Length == 0 && rand < (int)severity * 2))
                        {
                            for (var i = 0; i < UnityEngine.Random.Range(0, 2); i++)
                                AffectRandomSubstat(severity);
                        }
                        if (rand < (int)severity || (BldArray.Length == 0 && SubStatDict.Count == 0))
                        {
                            for (var i = 0; i < 3; i++)
                                AffectRandomResource(severity);
                        }
                        
                        break;
                    }

                // Affects: 3 buildings damage instance, possibly 5. 2 substats. 5 resource randoms.
                case RevoltSeverity.Severe:
                    {
                        if (BldArray.Length > 0)
                        {
                            for (var i = 0; i < UnityEngine.Random.Range(2, 5); i++)
                                AffectRandomBuilding(severity);
                        }
                        var rand = UnityEngine.Random.Range(0, 100);
                        if (rand < (int)severity || (BldArray.Length == 0 && rand < (int)severity * 2))
                        {
                            for (var i = 0; i < 2; i++)
                                AffectRandomSubstat(severity);
                        }
                        if (rand < (int)severity || (BldArray.Length == 0 && SubStatDict.Count == 0))
                        {
                            for (var i = 0; i < 5; i++)
                                AffectRandomResource(severity);
                        }
                        
                        break;
                    }
            }
            IPlayerNotification notify = Factory.CreatePlayerNotification();
            notify.RandomEventNotifyPlayer(this, new RevoltEventDetails()
            {
                RegionName = Region.Key,
                SubregionName = SubRegion.Key,
                BuildingDict = BldDict,
                SubregionStatDict = SubStatDict,
                ResourceDict = ResDict
            });
        }

        /// <summary>
        /// Calculates the severity of a revolt based on the revolt roll, subregion severity and
        /// difficulty modifier Modifiers are 0.0 - 2.0
        /// </summary>
        /// <returns> Revolt severity to be used for determining effects </returns>
        private RevoltSeverity CalculateSeverity()
        {
            var distance = (SubRegion.Value.SubRegionStats["RevoltRisk"] - RevoltRoll).RoundToTen();
            var subSeverity = SubRegion.Value.SubRegionStats["RevoltSeverity"];
            var difficultyModifier = GameData.GDInstance.GlobalVariables.Difficulty.RevoltSeverity;

            var severityNum = distance * subSeverity * difficultyModifier;

            // 0 - 30 inclusive
            if (severityNum <= (int)RevoltSeverity.Low)
                return RevoltSeverity.Low;

            // 30 exclusive - 60 inclusive
            else if (severityNum > (int)RevoltSeverity.Low && severityNum <= (int)RevoltSeverity.Medium)
                return RevoltSeverity.Medium;

            // 60 exclusive - 80 inclusive
            else if (severityNum > (int)RevoltSeverity.Medium && severityNum <= (int)RevoltSeverity.High)
                return RevoltSeverity.High;

            // > 80
            else return RevoltSeverity.Severe;
        }

        private bool HasRevolted(KeyValuePair<string, ISubRegion> subRegion, string name, TimeFrame timeFrame)
        {
            RevoltRoll = UnityEngine.Random.Range(1, 101);

            // 20% offset in favor of not revolting on weekly ticks.
            if (timeFrame == TimeFrame.Weekly)
            {
                RevoltRoll = (int)(RevoltRoll * 1.2);
            }
            
            if (RevoltRoll <= subRegion.Value.SubRegionStats["RevoltRisk"])
            {
                Region = new KeyValuePair<string, IRegion>
                             (name, GameData.GDInstance.Regions.regionDictionary[name]);
                SubRegion = subRegion;
                return true;
            }
            return false;
        }
    }
}

// TODO: Track number of revolts per sub and escalate the damage accordingly.
