﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts
{
    public enum RevoltSeverity
    {
        Low = 30,
        Medium = 60,
        High = 80,
        Severe = 100
    }
    //TODO: Revist #s during balancing
}

