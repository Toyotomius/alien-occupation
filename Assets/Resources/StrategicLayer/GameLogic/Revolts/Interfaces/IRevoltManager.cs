﻿using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;

// Added to its own namespace because Unity couldn't see it even though VStudio had no issues with build.
namespace AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts.Interfaces
{
    public interface IRevoltManager
    {
        void RevoltChecker(object sender, DateAndTime.TimeFrame e);
    }
}