﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIAnimations.GUIAnimationScripts
{
    /// <summary>
    /// Small script for boolean animation flags to use.
    /// </summary>
    public class TriggerBoolAnim
    {
        public void TriggerAnimation(GameObject parent, string boolName)
        {
            if (parent is object)
            {
                Animator animator = parent.GetComponent<Animator>();
                if (animator is object)
                {
                    var trigger = animator.GetBool(boolName);
                    animator.SetBool(boolName, !trigger);
                }
            }
        }
    }
}
