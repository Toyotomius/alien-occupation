﻿using UnityEngine;

public class EventUnreadAnimOff : MonoBehaviour
{
    /// <summary>
    /// Turns the unread animation off via OnClick, with the assumption that if you open or close the event panel
    /// you have read/acknowledged the event. 
    /// </summary>
    public void TurnUnreadAnimOff()
    {
        var animator = gameObject.GetComponent<Animator>();
        animator.SetBool("HasUnread", false);
    }
}
 