﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIAnimations.GUIAnimationScripts
{
    /// <summary>
    /// Script for opening and closing the in-game event notification panel.
    /// </summary>
    public class EventPanelAnimTrigger : MonoBehaviour
    {
        public void TriggerEventPanel()
        {
            GameObject Panel = this.gameObject;
            if (Panel is object)
            {
                var triggerBoolAnim = new TriggerBoolAnim();
                triggerBoolAnim.TriggerAnimation(Panel, "IsOpen");
            }
        }
    }
}
