﻿using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer
{
    /// <summary>
    /// Class for initializing main regions and their sub regions.
    /// </summary>
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    public class Region : IRegion
    {
        public Region(ISubRegion cen, ISubRegion western, ISubRegion eastern)
        {
            East = eastern;
            West = western;
            Central = cen;
            subRegions = new Dictionary<string, ISubRegion>()
            {
                {"East", East },
                {"West", West },
                {"Central", Central }
            };
        }

        public ISubRegion Central { get; set; }

        public string Description { get; set; }

        public ISubRegion East { get; set; }

        [JsonIgnore]
        public Dictionary<string, ISubRegion> subRegions { get; set; }

        public ISubRegion West { get; set; }
    }
}
