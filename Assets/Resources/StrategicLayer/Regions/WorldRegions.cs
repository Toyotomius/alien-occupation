﻿using AlienOccupation.Assets.Resources.Global;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer
{
    /// <summary> Class for instantiating all world regions. </summary>
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    public class WorldRegions
    {
        [JsonIgnore]
        public Dictionary<string, IRegion> regionDictionary;

        public WorldRegions(bool SetDefaults)
        {
            NorthAmerica = Factory.CreateRegion(SetDefaults);
            NorthAmerica.Description = "North American Region - \nIncludes all of North America";

            SouthAmerica = Factory.CreateRegion(SetDefaults);
            SouthAmerica.Description = "South America - \nIncludes all of South America";

            Africa = Factory.CreateRegion(SetDefaults);
            Africa.Description = "African Region - \nIncludes Africa and east to Istanbul";

            Europe = Factory.CreateRegion(SetDefaults);
            Europe.Description = "European Region - \nIncludes Europe, western half of Russia and west to Greenland";

            India = Factory.CreateRegion(SetDefaults);
            India.Description = "Indian Region - \nIncludes India, west to Istanbul and north to Russia";

            Asia = Factory.CreateRegion(SetDefaults);
            Asia.Description = "Asian Region - \nIncludes Asia and half of Russia";

            regionDictionary = new Dictionary<string, IRegion>()
            {
                {"NorthAmerica", NorthAmerica },
                {"SouthAmerica", SouthAmerica },
                {"Africa", Africa },
                {"Europe", Europe },
                {"India", India },
                {"Asia", Asia }
            };
        }

        public IRegion Africa { get; set; }

        public IRegion Asia { get; set; }

        public IRegion Europe { get; set; }

        public IRegion India { get; set; }

        public IRegion NorthAmerica { get; set; }

        public IRegion SouthAmerica { get; set; }

        // TODO: Create defaults for each region
        public void SetRegionDefaults()
        {
            var itsNull = GameData.GDInstance.GlobalVariables.Difficulty.RevoltSeverity;
            if (GameData.GDInstance.GlobalVariables.Difficulty.IsRandomStartingStats)
            {
            }
            else
            {
                foreach (var reg in regionDictionary)
                {
                    switch (reg.Key)
                    {
                        case "NorthAmerica":
                            {
                                reg.Value.Central["Happiness"] = 1000;
                                reg.Value.West["RevoltRisk"] = 100;
                                reg.Value.West["RevoltSeverity"] = 1.2;

                                //reg.Value.subRegions["Central"].BuildingSlots.Slot1 = new HarvestBuildingTest();
                                break;
                            }
                    }
                }
            }
        }
    }
}

//TODO: Look at this again
