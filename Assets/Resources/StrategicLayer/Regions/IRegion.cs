﻿using AlienOccupation.Assets.Resources.StrategicLayer.Regions;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer
{
    public interface IRegion
    {
        ISubRegion Central { get; set; }

        string Description { get; set; }

        ISubRegion East { get; set; }

        Dictionary<string, ISubRegion> subRegions { get; set; }

        ISubRegion West { get; set; }
    }
}
