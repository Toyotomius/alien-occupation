﻿using AlienOccupation.Assets.Resources.Global;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Regions
{
    public interface ISubRegion
    {
        BuildingSlots BuildingSlots { get; set; }

        bool RecentlyRevolted { get; set; }

        Dictionary<string, double> SubRegionStats { get; set; }

        double this[string key] { get; set; }
    }

    /// <summary> Sub region model for managing sub region stats. </summary>
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    public class SubRegion : ISubRegion
    {
        private double happiness;

        private int manpower;

        private double productivity;

        private double revoltRisk;

        private double revoltSeverity;

        public SubRegion(bool SetDefaults)
        {
            if (SetDefaults) { SetRegionDefaults(); }
            SubRegionStats = new Dictionary<string, double>()
            {
                {"Happiness", happiness },
                {"Manpower", manpower },
                {"Productivity", productivity },
                {"RevoltRisk", revoltRisk },
                {"RevoltSeverity", revoltSeverity }
            };
        }

        public BuildingSlots BuildingSlots { get; set; } = new BuildingSlots();

        public bool RecentlyRevolted { get; set; }

        //[JsonIgnore]
        public Dictionary<string, double> SubRegionStats { get; set; }

        // TODO: Flesh this out.
        public double this[string key]
        {
            get
            {
                return SubRegionStats[key];
            }
            set
            {
                switch (key)
                {
                    case "Manpower":
                        {
                            SubRegionStats[key] = value;
                            break;
                        }
                    default:
                        {
                            SubRegionStats[key] = Math.Round(value, 2, MidpointRounding.AwayFromZero);
                            break;
                        }
                }
            }
        }

        public void SetRegionDefaults()
        {
            happiness
                = Math.Round(50 * GameData.GDInstance.GlobalVariables.Difficulty.HappinessModifier, 2, MidpointRounding.AwayFromZero);
            productivity
                = Math.Round(
                50 * (GameData.GDInstance.GlobalVariables.Difficulty.ProductivityModifier / 1.7D)
                * (GameData.GDInstance.GlobalVariables.Difficulty.HappinessModifier / 1.3D),
                2,
                MidpointRounding.AwayFromZero);
            revoltRisk
                = Math.Round(10 * GameData.GDInstance.GlobalVariables.Difficulty.RevoltRisk * 2, 2, MidpointRounding.AwayFromZero);
            revoltSeverity
                = Math.Round(10 * GameData.GDInstance.GlobalVariables.Difficulty.RevoltSeverity * 2, 2, MidpointRounding.AwayFromZero);
        }
    }
}
