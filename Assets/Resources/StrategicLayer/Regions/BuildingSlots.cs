﻿using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AlienOccupation.Assets.Resources.StrategicLayer
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class BuildingSlots
    {
        [JsonIgnore]
        public Dictionary<string, IBuilding> BuildingSlotDict;

        public BuildingSlots()
        {
            BuildingSlotDict = new Dictionary<string, IBuilding>
            {
                { "Slot0", new EmptyBuilding() },
                { "Slot1", new EmptyBuilding() },
                { "Slot2", new EmptyBuilding() },
                { "Slot3", new EmptyBuilding() },
                { "Slot4", new EmptyBuilding() }
            };
        }

        //TODO: Revisit. Not pleased.
        [JsonProperty]
        public IBuilding Slot0
        {
            get
            {
                return BuildingSlotDict["Slot0"];
            }
            set
            {
                BuildingSlotDict["Slot0"] = value;
            }
        }

        [JsonProperty]
        public IBuilding Slot1
        {
            get
            {
                return BuildingSlotDict["Slot1"];
            }
            set
            {
                BuildingSlotDict["Slot1"] = value;
            }
        }

        [JsonProperty]
        public IBuilding Slot2
        {
            get
            {
                return BuildingSlotDict["Slot2"];
            }
            set
            {
                BuildingSlotDict["Slot2"] = value;
            }
        }

        [JsonProperty]
        public IBuilding Slot3
        {
            get
            {
                return BuildingSlotDict["Slot3"];
            }
            set
            {
                BuildingSlotDict["Slot3"] = value;
            }
        }

        [JsonProperty]
        public IBuilding Slot4
        {
            get
            {
                return BuildingSlotDict["Slot4"];
            }
            set
            {
                BuildingSlotDict["Slot4"] = value;
            }
        }

        
        
    }
}
