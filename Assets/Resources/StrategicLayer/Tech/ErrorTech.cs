﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech
{
    public class ErrorTech : ITech
    {
        public string CategoryName { get; set; } = "Error";

        public string Description { get; set; } = "This is an empty tech for any errors assigning relationships";

        public string DisplayName { get; set; } = "Error Tech";

        public int FavorCost { get; set; } = 0;

        public bool IsAvailable
        {
            get;
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "ErrorTech";

        public int Tier { get; set; } = -1;

        public void ApplyEffects()
        {
        }

        public void SetRelationships()
        {
        }
    }
}
