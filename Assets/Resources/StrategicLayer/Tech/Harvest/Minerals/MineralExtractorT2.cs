﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.Harvest
{
    public class MineralExtractorT2 : ITech
    {
        public string CategoryName { get; set; } = "Harvest";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Complex Mineral Extraction Techniques";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost
                && PrevTier.IsResearched;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "MineralExtractorT2";

        public int Tier { get; set; } = 2;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding(BuildingFactory.GetBuilding("ComplexMineralExtractor"));
        }

        public void SetRelationships()
        {
            PrevTier = GameData.GDInstance.Technology.RetrieveTech("MineralExtractorT1");
            NextTier = GameData.GDInstance.Technology.RetrieveTech("MineralExtractorT3");
        }
    }
}
