﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.Harvest
{
    public class MineralExtractorT3 : ITech
    {
        public string CategoryName { get; set; } = "Harvest";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Advanced Mineral Extraction Techniques";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost
                && PrevTier.IsResearched;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "MineralExtractorT3";

        public int Tier { get; set; } = 3;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding(BuildingFactory.GetBuilding("AdvancedMineralExtractor"));
        }

        public void SetRelationships()
        {
            PrevTier = GameData.GDInstance.Technology.RetrieveTech("MineralExtractorT2");
            NextTier = GameData.GDInstance.Technology.RetrieveTech("MineralExtractorT4");
        }
    }
}
