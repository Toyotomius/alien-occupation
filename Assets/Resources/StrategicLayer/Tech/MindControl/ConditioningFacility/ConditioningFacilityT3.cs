﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.MindControl
{
    public class ConditioningFacilityT3 : ITech
    {
        public string CategoryName { get; set; } = "Mind Control";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Complex Military Conditioning Techniques";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost
                       && PrevTier.IsResearched;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "ConditioningFacilityT3";

        public int Tier { get; set; } = 3;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding("AdvancedConditioningFacility");
        }

        public void SetRelationships()
        {
            NextTier = GameData.GDInstance.Technology.RetrieveTech("ConditioningFacilityT4");
            PrevTier = GameData.GDInstance.Technology.RetrieveTech("ConditioningFacilityT2");
        }
    }
}
