﻿using Newtonsoft.Json;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech
{
    public interface ITech
    {
        [JsonIgnore]
        string CategoryName { get; set; }
        [JsonIgnore]
        string Description { get; set; }
        [JsonIgnore]
        int FavorCost { get; set; }

        bool IsAvailable { get; }
        
        bool IsResearched { get; set; }
        [JsonIgnore]
        string DisplayName { get; set; }
        [JsonIgnore]
        ITech NextTier { get; set; }
        [JsonIgnore]
        ITech PrevTier { get; set; }

        [JsonIgnore]
        Sprite Sprite { get; set; }
        [JsonIgnore]
        int Tier { get; set; }
        [JsonIgnore]
        string TechName { get; set; }

        void ApplyEffects();
        void SetRelationships();
    }
}
