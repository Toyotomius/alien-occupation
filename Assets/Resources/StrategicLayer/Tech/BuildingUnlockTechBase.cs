﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech
{
    public interface BuildingUnlockTechBase
    {
        void UnlockBuilding();
    }
}
