﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.Suppression
{
    public class CheckpointsT3 : ITech
    {
        public string CategoryName { get; set; } = "Suppression";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Moderate Checkpoints Permit";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost
                       && PrevTier.IsResearched;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "CheckpointsT3";

        public int Tier { get; set; } = 3;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding("ModerateCheckpoints");
        }

        public void SetRelationships()
        {
            NextTier = GameData.GDInstance.Technology.RetrieveTech("CheckpointsT4");
            PrevTier = GameData.GDInstance.Technology.RetrieveTech("CheckpointsT2");
        }
    }
}
