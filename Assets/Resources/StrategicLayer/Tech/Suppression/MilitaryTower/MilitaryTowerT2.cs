﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.Suppression
{
    public class MilitaryTowerT2 : ITech
    {
        public string CategoryName { get; set; } = "Suppression";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Improved Tower Designs";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost
                       && PrevTier.IsResearched;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "MilitaryTowerT2";

        public int Tier { get; set; } = 2;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding("ModerateMilitaryTower");
        }

        public void SetRelationships()
        {
            NextTier = GameData.GDInstance.Technology.RetrieveTech("MilitaryTowerT3");
            PrevTier = GameData.GDInstance.Technology.RetrieveTech("MilitaryTowerT1");
        }
    }
}
