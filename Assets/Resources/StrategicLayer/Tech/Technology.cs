﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech
{
    [JsonObject(MemberSerialization = MemberSerialization.OptOut)]
    public class Technology
    {
        public Dictionary<string, ITech> Tech = new Dictionary<string, ITech>();

        public Technology()
        {
            var allTech = typeof(ITech);
            var allTechClasses = GetTechTypes(allTech);
            AddTypesToCollection(Tech, allTechClasses);
        }

        public ITech RetrieveTech(string techName)
        {
            if (!Tech.ContainsKey(techName))
            {
                UnityEngine.Debug.LogError($"{techName} does not exist in BuildingDict.");
                return new ErrorTech();
            }
            return Tech[techName];
        }

        private void AddTypesToCollection(Dictionary<string, ITech> coll, IEnumerable<Type> types)
        {
            foreach (var type in types)
            {
                ITech instancedType = (ITech)Activator.CreateInstance(type);
                if (instancedType.Tier < 0)
                {
                    continue;
                }
                coll.Add(instancedType.TechName, instancedType);
            }
        }

        private IEnumerable<Type> GetTechTypes(Type type)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && p.IsClass);
        }
    }
}

// TODO: Make it more mod friendly.
