﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.DefenseTech
{
    public interface IDefenseTech
    {
        int TechLevel { get; set; }
        string Name { get; set; }
    }
}
