﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.DefenseTech
{
    public class DefenseTechTest
    {
        int TechLevel { get; set; } = 1;
        public string Name { get; set; } = "DefenseTech mk1";
    }
}
