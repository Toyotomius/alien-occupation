﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.Manufacture
{
    public class WeaponsPlantT4 : ITech
    {
        public string CategoryName { get; set; } = "Manufacturing";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Master Weapons Construction";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost
                       && PrevTier.IsResearched;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "WeaponsPlantT4";

        public int Tier { get; set; } = 4;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding("MasterWeaponsPlant");
        }

        public void SetRelationships()
        {
            PrevTier = GameData.GDInstance.Technology.RetrieveTech("WeaponsPlantT3");
        }
    }
}
