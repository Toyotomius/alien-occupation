﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech.Manufacture
{
    public class DronePlantT1 : ITech
    {
        public string CategoryName { get; set; } = "Manufacturing";

        public string Description { get; set; } = "This is a test description";

        public string DisplayName { get; set; } = "Improved Drone Construction Techniques";

        public int FavorCost { get; set; } = 5;

        public bool IsAvailable
        {
            get
            {
                return GameData.GDInstance.PlayerResources["Favor"].Count >= FavorCost;
            }
        }

        public bool IsResearched { get; set; } = false;

        public ITech NextTier { get; set; }

        public ITech PrevTier { get; set; }

        public Sprite Sprite { get; set; } = Constants.HarvestBuildingTestSprite;

        public string TechName { get; set; } = "DronePlantT1";

        public int Tier { get; set; } = 1;

        public void ApplyEffects()
        {
            BuildingFactory.AddResearchedBuilding("ImprovedDronePlant");
        }

        public void SetRelationships()
        {
            NextTier = GameData.GDInstance.Technology.RetrieveTech("DronePlantT2");
        }
    }
}
