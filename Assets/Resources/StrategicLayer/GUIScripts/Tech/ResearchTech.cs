﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.Constants;

namespace AlienOccupation.Assets.Resources.StrategicLayer.Tech
{
    public class ResearchTech : MonoBehaviour
    {
        private IShowHide ShowHide;

        private ITech Tech;

        private GameObject TechTooltip;

        public void OnMouseEnter()
        {
            TechTooltip = GameObject.Instantiate(TechTooltipPrefab, Constants.StrategicLayerCanvas.transform);
            ShowHide.ShowGameObject(TechTooltip, 
                $"Name: {Tech.DisplayName}\n" +
                $"Tier: {Tech.Tier}\n" +
                $"Cost: {Tech.FavorCost}\n" +
                $"{Tech.Description}");
            if (Tech.IsResearched)
            {
                ShowHide.ShowGameObject(Tooltip.Inst.UITooltip, "Already Researched");
                var posScript = TechTooltip.GetComponent<SetTooltipPosition>();
                posScript.Adjustment = new Vector3(0, -25, 0);
                return;
            }
            if (GameData.GDInstance.PlayerResources["Favor"].Count < Tech.FavorCost)
            {
                ShowHide.ShowGameObject(Tooltip.Inst.UITooltip, "Insufficient Favor");
                var posScript = TechTooltip.GetComponent<SetTooltipPosition>();
                posScript.Adjustment = new Vector3(0, -25, 0);
            }
            
        }

        public void OnMouseExit()
        {
            GameObject.Destroy(TechTooltip);
            ShowHide.HideGameObject(Tooltip.Inst.UITooltip);
        }

        public void Research()
        {
            if (CheckRequirements(Tech))
            {
                Tech.IsResearched = true;

                // TODO: Change sprite
                this.GetComponent<Image>().color = Color.black;

                Tech.ApplyEffects();
                GameData.GDInstance.PlayerResources["Favor"].Count -= Tech.FavorCost;
                //Destroy(this.GetComponent<EventTrigger>());
                Destroy(TechTooltip);
            }
        }

        private bool CheckRequirements(ITech Tech)
        {
            return Tech.IsAvailable && !Tech.IsResearched;
        }

        // Start is called before the first frame update
        private void Start()
        {
            ShowHide = Factory.CreateShowHide();
            Tech = GameData.GDInstance.Technology.Tech.Values.Where(x => x.TechName == this.transform.GetChild(0).name).FirstOrDefault();
        }

        // Update is called once per frame
        private void Update()
        {
        }
    }
}
