﻿using AlienOccupation.Assets.Resources.Global;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.Constants;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.Tech
{
    public class DisplayTech : MonoBehaviour
    {
        private GameObject TechContent;

        private GameObject TechTabParent;

        /// <summary>
        /// Loops through the tech list and instantiates categories and tech cards as required.
        /// </summary>
        public void ShowTech()
        {
            var categoryList = new HashSet<string>();
            foreach (var tech in GameData.GDInstance.Technology.Tech)
            {
                categoryList.Add(tech.Value.CategoryName);
            }
            foreach (var cat in categoryList)
            {
                var tabGO = GameObject.Instantiate(TechTabPrefab, TechTabParent.transform);
                tabGO.name = cat;
                tabGO.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = cat;

                var GO = GameObject.Instantiate(TechCategoryPrefab, TechContent.transform);

                GO.name = cat;
            }

            // TODO: Separate same tier tech horizontally and attach the subsequent tier vertically

            foreach (var tech in GameData.GDInstance.Technology.Tech)
            {
                if (tech.Value.PrevTier is null)
                {
                    var category = tech.Value.CategoryName;
                    var current = tech.Value;
                    var parentGO = TechContent.transform.Find(current.CategoryName);
                    var GroupParent = GameObject.Instantiate(TechGroupParent, parentGO.transform);
                    while (current is object)
                    {
                        var GO = GameObject.Instantiate(TechCardPrefab, GroupParent.transform);
                        var techName = GO.transform.GetChild(0);
                        techName.name = current.TechName;
                        techName.GetComponent<TextMeshProUGUI>().text = current.DisplayName;
                        GO.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = current.Sprite;
                        GO.transform.GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>().text = current.Description;

                        if (current.IsResearched)
                        {
                            // TODO: Change sprite?
                            GO.GetComponent<Image>().color = Color.black;
                        }

                        current = current.NextTier;
                    }
                }
            }
        }

        public void Start()
        {
            TechContent = StrategicLayerCanvas.transform.Find("TechBackground/Scroll View/Viewport/TechContent").gameObject;
            TechTabParent = StrategicLayerCanvas.transform.Find("TechBackground/TabParent").gameObject;

            foreach (var tech in GameData.GDInstance.Technology.Tech.Values)
            {
                tech.SetRelationships();
            }
            ShowTech();
        }
    }
}
