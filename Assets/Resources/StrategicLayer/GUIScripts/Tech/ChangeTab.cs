﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.Tech
{
    /// <summary>
    /// ChangeTab script for changing tech category groups in UI
    /// </summary>
    public class ChangeTab : MonoBehaviour
    {
        //TODO: Change color to sprite change

        private TechGOs TechGOScript;

        /// <summary>
        /// Ensures all other groups are deactivated and changes image to default
        /// </summary>
        public void DeactivateAll()
        {
            foreach (var go in TechGOScript.GetTechGOs())
            {
                go.gameObject.SetActive(false);
            }
            foreach (var go in TechGOScript.GetTechTabs())
            {
                go.GetComponent<Image>().color = new Color32(29, 159, 159, 255);
            }
        }

        /// <summary>
        /// Activates the requested tech group and changes image to active
        /// </summary>
        public void SetActive()
        {
            var techGOs = TechGOScript.GetTechGOs();
            DeactivateAll();

            foreach (var transform in techGOs)
            {
                if (transform.name == this.name)
                {
                    transform.gameObject.SetActive(true);
                    this.gameObject.GetComponent<Image>().color = new Color32(65, 65, 65, 255);
                }
            }
        }

        public void Start()
        {
            TechGOScript = Constants.StrategicLayerCanvas.transform.Find("TechBackground/TabParent").GetComponent<TechGOs>();
        }
    }
}
