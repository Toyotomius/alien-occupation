﻿using AlienOccupation.Assets.Resources.Global;
using System.Collections.Generic;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.Tech
{
    /// <summary>
    /// Caches and returns tech groups in both TechContent and TechTab
    /// </summary>
    public class TechGOs : MonoBehaviour
    {
        private List<Transform> TechGOList = new List<Transform>();
        private List<Transform> TechTabList = new List<Transform>();
        private Transform TechContent;
        private Transform TabParent;

        public void Start()
        {
            TechContent = Constants.StrategicLayerCanvas.transform.Find("TechBackground/Scroll View/Viewport/TechContent");
            TabParent = Constants.StrategicLayerCanvas.transform.Find("TechBackground/TabParent");
        }
        public List<Transform> GetTechGOs()
        {
            if(TechGOList.Count == 0)
            {
                foreach(Transform trans in TechContent.transform)
                {
                    TechGOList.Add(trans);
                }
            }
            return TechGOList;
        }
        public List<Transform> GetTechTabs()
        {
            if (TechTabList.Count == 0)
            {
                foreach (Transform trans in TabParent.transform)
                {
                    TechTabList.Add(trans);
                }
            }
            return TechTabList;
        }
    }
}
