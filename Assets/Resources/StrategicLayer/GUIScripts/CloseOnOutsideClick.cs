﻿using AlienOccupation.Assets.Resources.Global;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// Uses pointer data to close menus when the left mouse is clicked outside of the menu script
    /// is attached to.
    /// </summary>
    public class CloseOnOutsideClick : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
    {
        private Coroutine RunningCoroutine;
        private IShowHide ShowHide;

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            if (RunningCoroutine is object)
            {
                StopCoroutine(RunningCoroutine);
            }
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            RunningCoroutine = StartCoroutine(OutsideClick(eventData));
        }

        public IEnumerator OutsideClick(PointerEventData eventData)
        {
            // Temporary fix until todo is completed
            if (GameObject.Find("BuildListSlot") is object && this.gameObject.name != "BuildListSlot")
            {
                yield break;
            }
            while (this.gameObject.activeSelf)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    ShowHide = Factory.CreateShowHide();
                    ShowHide.HideGameObject(this.gameObject);
                    yield break;
                }

                yield return null;
            }
        }
    }
}

//TODO: Make it back out of menus one at a time instead of everything at once
