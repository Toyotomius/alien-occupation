﻿using System;
using UnityEngine;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.GameData;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// Handles time changing in the game. This is important as most story and random events are
    /// based on this information. Also displays the time in-game by modifying the DateAndTime game object.
    /// </summary>
    public class DateAndTime : MonoBehaviour
    {
        private bool IsSunday = true;
        private DateTime PreviousDay;
        private DateTime PreviousMonth;
        private DateTime PreviousWeek;
        private DateTime PreviousYear;
        private Text Text;

        public event EventHandler<TimeFrame> DayChange;

        public event EventHandler<TimeFrame> MonthChange;

        public event EventHandler<TimeFrame> WeekChange;

        public event EventHandler<TimeFrame> YearChange;

        public enum TimeFrame
        {
            Daily,
            Weekly,
            Monthly,
            Yearly
        }

        // Start is called before the first frame update
        public void Start()
        {
            PreviousDay = GDInstance.GlobalVariables.Date;
            PreviousMonth = GDInstance.GlobalVariables.Date;
            PreviousWeek = GDInstance.GlobalVariables.Date;
            PreviousYear = GDInstance.GlobalVariables.Date;
            Text = GetComponent<Text>();
            if (PreviousWeek.DayOfWeek != DayOfWeek.Sunday)
            {
                var difference = (int)GDInstance.GlobalVariables.Date.DayOfWeek;
                PreviousWeek = PreviousWeek.AddDays(-difference);
            }
        }

        protected virtual void OnDayChange(TimeFrame e)
        {
            EventHandler<TimeFrame> handler = DayChange;
            handler?.Invoke(this, e);
        }

        protected virtual void OnMonthChange(TimeFrame e)
        {
            EventHandler<TimeFrame> handler = MonthChange;
            handler?.Invoke(this, e);
        }

        protected virtual void OnWeekChange(TimeFrame e)
        {
            EventHandler<TimeFrame> handler = WeekChange;
            handler?.Invoke(this, e);
        }

        protected virtual void OnYearChange(TimeFrame e)
        {
            EventHandler<TimeFrame> handler = YearChange;
            handler?.Invoke(this, e);
        }

        // Update is called once per frame
        private void Update()
        {
            Text.text = GDInstance.GlobalVariables.Date.ToString("yyyy-MM-dd HH:mm:ss");
            GDInstance.GlobalVariables.Date = GDInstance.GlobalVariables.Date.AddDays(Time.deltaTime);
            if (IsSunday && GDInstance.GlobalVariables.Date.DayOfWeek >= DayOfWeek.Monday)
            {
                IsSunday = false;
            }
            if (GDInstance.GlobalVariables.Date.DayOfWeek == PreviousWeek.DayOfWeek && !IsSunday)
            {
                IsSunday = true;
                PreviousWeek = GDInstance.GlobalVariables.Date;
                OnWeekChange(TimeFrame.Weekly);
            }
            if (GDInstance.GlobalVariables.Date.Month != PreviousMonth.Month)
            {
                PreviousMonth = GDInstance.GlobalVariables.Date;
                OnMonthChange(TimeFrame.Monthly);
            }
            if (GDInstance.GlobalVariables.Date.Day != PreviousDay.Day)
            {
                PreviousDay = GDInstance.GlobalVariables.Date;
                OnDayChange(TimeFrame.Daily);
            }
            if (GDInstance.GlobalVariables.Date.Year > PreviousYear.Year)
            {
                PreviousYear = GDInstance.GlobalVariables.Date;
                OnYearChange(TimeFrame.Yearly);
            }
        }
    }
}
