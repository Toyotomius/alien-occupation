﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    public class FocusCameraRegion : MonoBehaviour
    {
        /// <summary>
        /// Script used by playernotification to rotate the camera around the Earth to point at the
        /// specific region clicked.
        /// </summary>
        public void FocusOnRegion()
        {
            var camera = GameObject.Find("Main Camera");
            var earth = GameObject.Find("Earth");
            var region = this.gameObject.name.Replace(" ", string.Empty);
            var targetRegion = earth.transform.Find(region);

            var cameraCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, Camera.main.nearClipPlane));
            RaycastHit hit;
            Physics.Raycast(cameraCenter, camera.transform.forward, out hit, 1000);

            if (hit.transform.gameObject == targetRegion.gameObject)
            {
                // Do nothing
            }
            else
            {
                var script = camera.GetComponent<CameraLook>();
                script.cameraLook = true;
                //camera.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                camera.transform.position = new Vector3(0, 0, -40);

                script.rotation = Quaternion.FromToRotation(camera.transform.position, targetRegion.position);
                // Quick fix to keep the z zeroed out when clicking a notification.
                script.rotation = Quaternion.Euler(new Vector3(script.rotation.eulerAngles.x, script.rotation.eulerAngles.y, 0));

                script.cameraLook = false;
            }
            Physics.SyncTransforms();
        }
    }
}
