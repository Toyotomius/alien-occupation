﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.Global.Extensions;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.VariableNames;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.Regions
{
    /// <summary>
    /// Script attached to building tier buttons in the buildables list. Creates required GOs based
    /// on building tier button activated
    /// </summary>
    public class ShowBuildablesByTier : MonoBehaviour
    {
        public void RefreshBuildables()
        {
            foreach (Transform button in this.transform.parent)
            {
                //TODO: Change to sprite.
                button.GetComponent<Image>().color = Color.white;
            }
            string buildableBuildings = Names.GameObjects.BuildableContainerForCategoryGO.Value;
            Transform buildingContainerGO = this.transform.ParentSiblingByName(buildableBuildings);
            foreach (Transform building in buildingContainerGO)
            {
                GameObject.Destroy(building.gameObject);
            }
            ShowBuildables();
        }

        public void ShowBuildables()
        {
            //TODO: Change to sprite.
            this.transform.GetComponent<Image>().color = Color.blue;
            var researchBuildings = BuildingFactory.GetResearchedBuildings();
            var orderedBldList = researchBuildings.Values
                                 .Where(x => x.BuildingType == this.transform.parent.parent.name)
                                 .Where(y => $"Tier{y.BuildingTier}" == this.name)
                                 .Select(x => x).ToList();

            string buildableBuildings = Names.GameObjects.BuildableContainerForCategoryGO.Value;
            Transform buildingContainerGO = this.transform.ParentSiblingByName(buildableBuildings);

            foreach (IBuilding building in orderedBldList)
            {
                var bldParent = UnityEngine.GameObject.Instantiate(Constants.BuildingParent, buildingContainerGO.transform);
                var blding = UnityEngine.GameObject.Instantiate(Constants.BuildingPrefab, bldParent.transform);
                blding.transform.SetAsFirstSibling();

                var descGO = bldParent.transform.GetChild(1);
                descGO.GetComponent<TextMeshProUGUI>().text = $"{building.DisplayName}\n{building.Description}";
                descGO.name = building.Name;

                blding.GetComponent<Image>().sprite = building.BuildingSprite;
                blding.name = building.Name;

                string BuildingCostGO = Names.GameObjects.BuildingCostGO.Value;
                var costText = blding.transform.Find(BuildingCostGO).GetComponent<TextMeshProUGUI>();
                costText.text = building.Cost.ToString();

                if (building.Cost > GameData.GDInstance.PlayerResources["Materials"].Count)
                {
                    blding.GetComponent<Image>().color = new Color32 { a = 255, b = 51, g = 51, r = 51 };
                    costText.color = Color.red;
                }
            }
        }
    }
}
