﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// Script for when one selects a region game object on the earth (regional capital objects
    /// denoted by continent.
    /// </summary>
    public class RegionSelection : MonoBehaviour
    {
        private Transform[] CentralPanels;
        private float defaultPanelWidth;
        private Transform[] EasternPanels;
        private Text RegionName;
        private GameObject RegionOverlay;
        private IShowHide ShowHide;
        private Transform[] WesternPanels;

        /// <summary> If left mouse button is selected and NOT over a UI element </summary>
        public void OnMouseOver()
        {
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && !RegionOverlay.activeSelf)
            {
                ShowRegionWindow(this.gameObject.name);
            }
        }

        public void Start()
        {
            RegionOverlay = GameObject.Find("StrategicLayerCanvas").transform.Find("RegionOverlayBackground").gameObject;
            RegionName = RegionOverlay.transform.Find("Region Name").GetComponent<Text>();
            ShowHide = Factory.CreateShowHide();
            WesternPanels
               = RegionOverlay.transform.Find("West").GetComponentsInChildren<Transform>(true).Where(x => x.name == "Percentage").ToArray();
            CentralPanels
                = RegionOverlay.transform.Find("Central").GetComponentsInChildren<Transform>(true).Where(x => x.name == "Percentage").ToArray();
            EasternPanels
                = RegionOverlay.transform.Find("East").GetComponentsInChildren<Transform>(true).Where(x => x.name == "Percentage").ToArray();
            defaultPanelWidth
                = RegionOverlay.transform.Find("West").Find("StatsPanelParent").Find("Happiness").Find("Happiness").GetComponent<RectTransform>().rect.width;
        }
        /// <summary>
        /// Displays buildings in the subregion
        /// </summary>
        /// <param name="name">Name of the region</param>
        private void ShowBuildings(string name)
        {
            
            foreach (var sub in GameData.GDInstance.Regions.regionDictionary[name].subRegions)
            {
                var parent = RegionOverlay.transform
                        .transform.Find($"{sub.Key}")
                        .GetComponentsInChildren<Transform>(true)
                        .Where(x => x.name == "RegionalBuildingSlotParent").FirstOrDefault();
                var buildings = parent.GetComponentsInChildren<Transform>().ToArray();
                if (buildings.Length > 0)
                {
                    foreach (Transform building in buildings)
                    {
                        // Check is required because apparently GetComponentsInChildren also gets it in the current GameObject.
                        if (building.name != "RegionalBuildingSlotParent")
                        {
                            Destroy(building.gameObject);
                        }
                    }
                }
                var i = 0;
                foreach (IBuilding building in sub.Value.BuildingSlots.BuildingSlotDict.Values)
                {
                    var instance = UnityEngine.GameObject.Instantiate(Constants.BuildingSlot, parent.transform);

                    instance.name = $"Slot{i}";
                    i++;

                    instance.GetComponent<Image>().sprite = building.BuildingSprite;
                }
            }
        }

        /// <summary> Loads the regional window at mouse position and all informational displays. </summary>
        /// <param name="name"> Name of the region </param>
        //TODO: Budge the window if loads off screen.
        private void ShowRegionWindow(string name)
        {
            RegionOverlay.transform.position = Input.mousePosition;
            ShowHide.ShowGameObject(RegionOverlay);

            RegionName.text = GameData.GDInstance.Regions.regionDictionary[name].Description;
            RegionName.name = name;


            foreach (var itm in WesternPanels)
            {
                var rectTransform = itm.GetComponent<RectTransform>();
                var subRegionStat = GameData.GDInstance.Regions.regionDictionary[name]
                                    .West[itm.transform.parent.name];

                rectTransform.SetSizeWithCurrentAnchors(
                    RectTransform.Axis.Horizontal,
                    defaultPanelWidth * ((float)subRegionStat / 100));

                itm.transform.parent.GetComponentInChildren<Text>().text
                    = $"{subRegionStat}";
            }
            foreach (var itm in CentralPanels)
            {
                var rectTransform = itm.GetComponent<RectTransform>();
                var subRegionStat = GameData.GDInstance.Regions.regionDictionary[name]
                                    .Central[itm.transform.parent.name];

                rectTransform.SetSizeWithCurrentAnchors(
                    RectTransform.Axis.Horizontal,
                    defaultPanelWidth * ((float)subRegionStat / 100));

                itm.transform.parent.GetComponentInChildren<Text>().text
                    = $"{subRegionStat}";
            }
            foreach (var itm in EasternPanels)
            {
                var rectTransform = itm.GetComponent<RectTransform>();
                var subRegionStat = GameData.GDInstance.Regions.regionDictionary[name]
                                    .East[itm.transform.parent.name];

                rectTransform.SetSizeWithCurrentAnchors(
                    RectTransform.Axis.Horizontal,
                    defaultPanelWidth * ((float)subRegionStat / 100));

                itm.transform.parent.GetComponentInChildren<Text>().text
                    = $"{subRegionStat}";
            }
            ShowBuildings(name);
        }
    }
}
