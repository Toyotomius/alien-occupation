﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    public class ShowBuildables : MonoBehaviour
    {
        private IBuilding Building;
        private GameObject BuildingTooltip;
        private string RegionName;
        private IShowHide ShowHide;
        private string SubRegionName;
        

        public void DisplayBuildingTooltip()
        {
            BuildingTooltip = GameObject.Instantiate(Constants.BuildingTooltipPrefab, Constants.StrategicLayerCanvas.transform);
            BuildingTooltip.name = "BuildingTooltip";

            Building = GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegionName].BuildingSlots.BuildingSlotDict[this.name];

            ShowHide.ShowGameObject(BuildingTooltip, $"{Building.DisplayName}\n" +
                $"Cost: {Building.Cost}\nCurrent HP: {Building.CurrentHitpoints}\n\n{Building.Description}");
        }

        public void RemoveTooltip()
        {
            GameObject.Destroy(BuildingTooltip);
        }

        public void ShowBuildingList()
        {
            var buildList = Constants.StrategicLayerCanvas.transform.Find("BuildListSlot");
            if (buildList is object)
            {
                GameObject.Destroy(buildList.gameObject);
            }
            
            buildList = GameObject.Instantiate(Constants.BuildListSlotPrefab, Constants.StrategicLayerCanvas.transform).transform;
            buildList.gameObject.name = "BuildListSlot";
            Constants.RegionOverlay.GetComponent<RegionalVariables>().SlotForBuild = this.transform.name;
            Constants.RegionOverlay.GetComponent<RegionalVariables>().SubRegion = SubRegionName;

            RemoveTooltip();
        }

        public void Start()
        {
            ShowHide = Factory.CreateShowHide();
            var subRegion = this.transform.parent.parent.parent;
            SubRegionName = subRegion.name;
            RegionName = subRegion.parent.GetChild(0).name;
        }
    }
}
