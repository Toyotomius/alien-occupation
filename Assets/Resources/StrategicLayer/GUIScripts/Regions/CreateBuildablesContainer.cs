﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.Global.Extensions;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.Regions;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using static AlienOccupation.Assets.Resources.Global.VariableNames;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    public class CreateBuildablesContainer : MonoBehaviour
    {
        private Transform Parent;

        private void OnDisable()
        {
            foreach (Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        /// <summary>
        /// Creates the containers for buildable buildings gameobjects each time a region is activated.
        /// </summary>
        private void OnEnable()
        {
            Parent = this.transform;
            Dictionary<string, IBuilding> researchBuildings = BuildingFactory.GetResearchedBuildings();
            var orderedBldList = researchBuildings
                .Values.OrderBy(x => x.BuildingType).ThenBy(x => x.BuildingTier).ToList();
            var buildingCategories = orderedBldList.Select(x => x.BuildingType).Distinct().ToList();
            var catGOList = new List<GameObject>();

            string buildableContent = Names.GameObjects.BuildableScrollViewContent.Value;

            foreach (var cat in buildingCategories)
            {
                var catGO = UnityEngine.GameObject.Instantiate(Constants.BuildingCategoryParentPrefab,
                    this.transform.DepthByName(buildableContent)); // BuildingContent object in Scrollview
                catGO.name = cat;
                catGO.GetComponent<TextMeshProUGUI>().text = cat;

                catGOList.Add(catGO);
            }
            var tier = -1;

            foreach (IBuilding building in orderedBldList)
            {
                GameObject catGO = catGOList.Find(x => x.name == building.BuildingType);

                tier = building.BuildingTier;
                if (catGO.transform.GetChild(0).Find($"Tier{tier}") == null)
                {
                    var tierButtonGO = UnityEngine.GameObject.Instantiate(Constants.BuildingTierButton,
                        catGO.transform.GetChild(0).transform); // Tier button container
                    tierButtonGO.name = $"Tier{tier}";
                    tierButtonGO.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = tier.ToString();
                    if (tierButtonGO.name == Names.GameObjects.BaseBuildingTier.Value)
                    {
                        tierButtonGO.GetComponent<ShowBuildablesByTier>().ShowBuildables();
                    }
                }
            }
        }
    }
}
