﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;
using static AlienOccupation.Assets.Resources.Global.VariableNames;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.Regions
{
    /// <summary> Class used for determining and beginning construction of game buildings. </summary>
    public class Construction : MonoBehaviour
    {
        private IBuilding Building;
        private GameObject BuildingTooltip;
        private string RegionName;
        private IShowHide ShowHide;
        private string Slot;
        private readonly Transform SubRegion;
        private string SubRegionName;

        /// <summary>
        /// If requirements are met for the building sends building to BuildingManager to be built
        /// in the appropriate slot
        /// </summary>
        public void Build()
        {
            if (CheckRequirements(Building))
            {
                var manager = new BuildingManager();
                Building.SubRegionLocation = GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegionName];
                GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegionName].BuildingSlots.BuildingSlotDict[Slot] = manager.Add(Building);

                //switch (Slot)
                //{
                //    case "Slot0":
                //        GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegion.name].BuildingSlots.BuildingSlotDict[Slot] = manager.Add(Building);
                //        break;

                // case "Slot1": GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegion.name].BuildingSlots.Slot1
                // = manager.Add(Building); break;

                // case "Slot2": GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegion.name].BuildingSlots.Slot2
                // = manager.Add(Building); break;

                // case "Slot3": GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegion.name].BuildingSlots.Slot3
                // = manager.Add(Building); break;

                //    case "Slot4":
                //        GameData.GDInstance.Regions.regionDictionary[RegionName].subRegions[SubRegion.name].BuildingSlots.Slot4 = manager.Add(Building);
                //        break;
                //}
            }
            string slotBuildableList = Names.GameObjects.BuildListForSelectedSlot.Value;
            ShowHide.HideGameObject(GameObject.Find(slotBuildableList));

            // Quick fix to make sure the tooltips are gone. See TODO.
            OnMouseExit();
        }

        /// <summary>
        /// Displays building tooltips on mouse enter of building object. Insufficient Resources
        /// tooltip displayed if CheckRequirements returns false.
        /// </summary>
        public void OnMouseEnter()
        {
            // Unity parent GO collider is used for child GO. This destroys the tooltip that gets created by the parent first
            // to prevent double tooltips.
            //TODO: Maybe not this? Alternative is to raycast from camera but I don't want to here.

            GameObject.Destroy(GameObject.Find(Names.GameObjects.BuildingTooltip.Value));

            BuildingTooltip = GameObject.Instantiate(Constants.TooltipPrefab, Constants.StrategicLayerCanvas.transform);
            BuildingTooltip.name = Names.GameObjects.BuildingTooltip;

            SetTooltipPosition posScript = BuildingTooltip.GetComponent<SetTooltipPosition>();

            if (!CheckRequirements(Building))
            {
                ShowHide.ShowGameObject(Tooltip.Inst.UITooltip, "Insufficient Resources");
                posScript.Adjustment = new Vector3(0, -25, 0);
            }
            ShowHide.ShowGameObject(BuildingTooltip, $"{Building.DisplayName}\nCost: {Building.Cost}\n\n{Building.Description}");
        }

        /// <summary>
        /// Hides standard tooltip (used for insufficient resources) and destroys BuildingTooltip on
        /// mouse exit.
        /// </summary>
        //TODO: Fix bug when clicking on building that does not meet requirements. Menu disappears and MouseExit doesn't trigger.
        public void OnMouseExit()
        {
            ShowHide.HideGameObject(Tooltip.Inst.UITooltip);
            GameObject.Destroy(BuildingTooltip);
        }

        public void Start()
        {
            Building = BuildingFactory.GetBuilding(this.name);
            ShowHide = Factory.CreateShowHide();

            RegionName = Constants.RegionOverlay.transform.GetChild(0).name;

            SubRegionName = Constants.RegionOverlay.GetComponent<RegionalVariables>().SubRegion;

            //while (SubRegion.name != "West" && SubRegion.name != "Central" && SubRegion.name != "East")
            //{
            //    SubRegion = SubRegion.parent;
            //    if (SubRegion.name == this.transform.root.name)
            //    {
            //        break;
            //    }
            //}
            Slot = Constants.RegionOverlay.GetComponent<RegionalVariables>().SlotForBuild;
        }

        private bool CheckRequirements(IBuilding building)
        {
            return GameData.GDInstance.PlayerResources[Names.ConsumableResource.ConsumableMaterial.Value].Count >= building.Cost;
        }
    }
}
