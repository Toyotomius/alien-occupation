﻿using AlienOccupation.Assets.Resources;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using UnityEngine;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.GameData;

public class UpdateTest : MonoBehaviour
{
    /// <summary> Just a test file for checking unity behaviors. Will be removed. </summary>
    //TODO: Remove this. Ditch it. Get rid of it. Burn it with fire. Nuke it from orbit. Leave none alive.
    // Start is called before the first frame update

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            var TechMenu = GameObject.Find("StrategicLayerCanvas").transform.Find("TechBackground").gameObject;
            TechMenu.transform.GetComponent<ContentSizeFitter>().enabled = false;
            TechMenu.transform.GetComponent<ContentSizeFitter>().enabled = true; 
            Canvas.ForceUpdateCanvases();
            //foreach(var tech in GameData.GDInstance.Technology.Tech.Values)
            //{
            //    tech.ApplyEffects();
            //}
            //foreach(var build in BuildingFactory.GetResearchedBuildings().Values)
            //{
            //    UnityEngine.Debug.Log(build.Name);
            //}
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            var manager = new BuildingManager();

            GDInstance.Regions.NorthAmerica.Central.BuildingSlots.Slot1 =
                manager.CancelBuilding(GDInstance.Regions.NorthAmerica.Central.BuildingSlots.Slot1);
        }
    }
}
