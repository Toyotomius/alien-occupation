﻿using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Script for activating and deactivating various in-game objects/windows/panels. </summary>
    public class ShowHide : IShowHide
    {
        public void HideGameObject(GameObject gameObject)
        {
            gameObject.SetActive(false);
        }

        public void ShowGameObject(GameObject gameObject, string gameObjectText)
        {
            var text = gameObject.GetComponentInChildren<Text>();
            text.text = gameObjectText;
            gameObject.SetActive(true);
        }

        public void ShowGameObject(GameObject gameObject)
        {
            gameObject.SetActive(true);
        }
    }
}
