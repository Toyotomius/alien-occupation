﻿using UnityEngine;

public interface ISetTooltipPosition
{
    Vector3 GetUITooltipPosition();
}