﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    public interface IShowHide
    {
        void HideGameObject(GameObject gameObject);

        void ShowGameObject(GameObject gameObject, string gameObjectText);

        void ShowGameObject(GameObject gameObject);
    }
}
