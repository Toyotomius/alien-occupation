﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Opens the requested sub menu and makes main canvas noninteractable. </summary>
    public class OpenMenuItem : MonoBehaviour
    {
        public void OpenSubMenu(GameObject subMenuGO)
        {
            IShowHide showHide = Factory.CreateShowHide();

            this.GetComponent<CanvasGroup>().interactable = false;
            showHide.ShowGameObject(subMenuGO);
        }
    }
}
