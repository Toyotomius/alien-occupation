﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Toggles the esc menu in the Strategic Layer Scene </summary>
    public class ToggleESCMenu : MonoBehaviour
    {
        private GameObject Canvas;
        private GameObject EscMenu;
        private IShowHide ShowHide;

        public void OpenESCMenu()
        {
            if (EscMenu.activeSelf) { ShowHide.HideGameObject(EscMenu); }
            else
            {
                Time.timeScale = 0;
                ShowHide.ShowGameObject(EscMenu);
                EscMenu.transform.SetAsLastSibling();
            }
        }

        private void Start()
        {
            Canvas = GameObject.Find("StrategicLayerCanvas");
            EscMenu = Canvas.transform.Find("ESCMenuParent").gameObject;
            ShowHide = Factory.CreateShowHide();
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OpenESCMenu();
            }
        }
    }
}
