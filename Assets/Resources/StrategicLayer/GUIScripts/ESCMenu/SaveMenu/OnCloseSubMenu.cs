﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// Script to reactivate previous/parent menu based on scene when sub menu is closed.
    /// </summary>
    public class OnCloseSubMenu : MonoBehaviour
    {
        /// <summary>
        /// Finds appropriate menu based on scene and re-activates the previously disabled menu.
        /// Previous menu had interactable set to false to prevent clicking while sub menu is open.
        /// </summary>
        private GameObject disabledMenu;

        public void ActivateESCMenu()
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "StrategicLayer":
                    {
                        disabledMenu = GameObject.Find("ESCMenuBackground").gameObject;
                        break;
                    }
                case "GameStartMenu":
                    {
                        disabledMenu = GameObject.Find("MainMenuBackground").gameObject;
                        break;
                    }
            }

            disabledMenu.GetComponent<CanvasGroup>().interactable = true;
            OnClickSaveFile.FileName = "";
        }

        //TODO: Make a bit more generic for use with menus other than the ESC menu.
    }
}
