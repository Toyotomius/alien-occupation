﻿using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// OnClick handling for when someone clicks on an existing save file. If the GameObject is not
    /// null, script then puts the name of the save file into the save name input field to preserve
    /// file name for overwrite.
    /// </summary>
    public class OnClickSaveFile
    {
        public static string FileName;

        public void SelectSaveFile(string fileName)
        {
            FileName = fileName;
            if (GameObject.Find("SaveNameInputField") != null)
            {
                var inputField = GameObject.Find("SaveNameInputField").GetComponent<InputField>();
                inputField.text = FileName;
            }
        }
    }
}
