﻿using AlienOccupation.Assets.Resources.Global;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Script for when clicking Save in a save file or load file menu. </summary>
    public class Save : MonoBehaviour
    {
        private IFileHandling FileHandling;

        /// <summary> OnClick for the Save button. Runs the save coroutine. </summary>
        public void OnSaveClick()
        {
            StartCoroutine(SaveCoroutine());
        }

        /// <summary> Coroutine for when Save is selected. Used for confirmation dialog </summary>
        /// <returns>
        /// null. Yield return null is here to await enum change for confirmation popup. Once an
        /// option is selected (Yes or No) the variable dialog value changes, completing the dialog.
        /// </returns>
        public IEnumerator SaveCoroutine()
        {
            FileHandling = Factory.CreateFileHandling();

            var fileName = GameObject.Find("SaveNameText").GetComponent<Text>().text;

            if (File.Exists($@"{Application.persistentDataPath}\saves\{fileName}.sav"))
            {
                ConfirmationDialog dialog = ShowDialog();
                while (dialog.result == DialogResults.None)
                {
                    yield return null;
                }
                if (dialog.result == DialogResults.Yes)
                {
                    FileHandling.Save(fileName);
                    GameObject.Find(fileName).transform.SetAsFirstSibling();
                    HideDialog();
                }
                else
                {
                    HideDialog();
                }
                dialog.result = DialogResults.None;
            }
            else
            {
                FileHandling.Save(fileName);
                var onEnable = transform.GetComponentInParent<SetOnEnable>();
                onEnable.OnEnable();
                GameObject.Find(fileName).transform.SetAsFirstSibling();
            }
        }

        private void HideDialog()
        {
            var confirmDialog = GameObject.Find("SaveConfirmation");
            confirmDialog.SetActive(false);
        }

        private ConfirmationDialog ShowDialog()
        {
            var saveConfParent = GameObject.Find("Save");
            var confirmDialog = saveConfParent.transform.Find("SaveConfirmation").gameObject;
            confirmDialog.SetActive(true);
            return confirmDialog.GetComponent<ConfirmationDialog>();
        }
    }
}
