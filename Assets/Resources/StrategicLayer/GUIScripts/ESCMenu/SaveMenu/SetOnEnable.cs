﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.GameData;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Script that handles displaying the save files and making them clickable GameObjects. </summary>
    public class SetOnEnable : MonoBehaviour
    {
        /// <summary> Automatically populates the save name input field for new save files. </summary>
        public void OnEnable()
        {
            if (this.transform.name == "SaveMenu")
            {
                GameObject.Find("SaveNameInputField").GetComponent<InputField>().text =
                           $"{DateTime.Now.ToString("yyyy-MM-dd T HH.mm.ss")} - {GDInstance.GlobalVariables.Date.ToString("yyyy-MM-dd T HH.mm.ss")}";
            }

            var saveFiles = GetSaves();
            DisplaySaves(saveFiles);
        }

        /// <summary>
        /// Uses PointEventData to determine when save file is clicked to highlight file.
        /// Note: I used this as an alternative to test viability for easier ports to mobile devices.
        /// </summary>
        /// <param name="data"> Event data for pointer actions </param>
        public void OnSelectDelegate(PointerEventData data)
        {
            var text = data.selectedObject.GetComponent<Text>();
            var fileName = text.text;

            var onClick = new OnClickSaveFile();
            onClick.SelectSaveFile(fileName);
        }

        /// <summary>
        /// Adds Selectable component to save file gameobject and sets the color to red when selected.
        /// </summary>
        /// <param name="savesGO"> Save file game object </param>
        private static void SetSelectable(GameObject savesGO)
        {
            savesGO.AddComponent<Selectable>();
            var selectable = savesGO.GetComponent<Selectable>();
            var colors = selectable.colors;
            colors.selectedColor = Color.red;
            selectable.colors = colors;
        }

        /// <summary>
        /// Sets the text of the save file game object to the save file name for display.
        /// </summary>
        /// <param name="font">     The font used </param>
        /// <param name="fileName"> The file name of the save file, minus extension </param>
        /// <param name="savesGO">  Game object of the save file </param>
        private static void SetText(Font font, string fileName, GameObject savesGO)
        {
            savesGO.AddComponent<Text>();
            var text = savesGO.GetComponent<Text>();
            text.font = font;
            text.fontSize = 12;
            text.alignment = (TextAnchor)TextAlignment.Left;
            text.text = fileName;
        }

        /// <summary>
        /// Takes all save file names and creates individual, selectable GameObjects and calls
        /// individual methods for display.
        /// </summary>
        /// <param name="saveFileNames"> Array of all the save file names, minus extension </param>
        private void DisplaySaves(string[] saveFileNames)
        {
            var scrollContent = GameObject.Find("SFContent");

            var arial = (Font)UnityEngine.Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");

            foreach (var name in saveFileNames)
            {
                if (GameObject.Find($"{name}") != null) { continue; }
                GameObject savesGO = new GameObject($"{name}");
                savesGO.transform.parent = scrollContent.transform;

                SetText(arial, name, savesGO);

                SetSelectable(savesGO);

                SetEvents(savesGO);
            }
        }

        /// <summary> Gets all .sav files from save folder. Orders descending by last write. </summary>
        /// <returns> String array of file names without extensions </returns>
        private string[] GetSaves()
        {
            string[] saveFiles = Directory
                                    .GetFiles($@"{Application.persistentDataPath}\saves\", "*.sav")
                                    .OrderByDescending(File.GetLastWriteTime)
                                    .Select(Path.GetFileNameWithoutExtension)
                                    .ToArray();
            return saveFiles;
        }

        /// <summary>
        /// Adds an event to each save file game object for on-select. Did this as an experient in
        /// using PointerEventData.
        /// </summary>
        /// <param name="savesGO"> Save file game object </param>
        private void SetEvents(GameObject savesGO)
        {
            savesGO.AddComponent<EventTrigger>();
            EventTrigger trigger = savesGO.GetComponent<EventTrigger>();
            var entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Select;
            entry.callback.AddListener((data) => { OnSelectDelegate((PointerEventData)data); });
            trigger.triggers.Add(entry);
        }
    }
}
