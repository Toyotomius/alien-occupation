﻿using AlienOccupation.Assets.Resources.Global;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Script for when clicking Delete in a save file or load file menu. </summary>
    public class OnDeleteClick : MonoBehaviour
    {
        private IFileHandling fileHandling;

        /// <summary>
        /// Coroutine for when delete is selected. Uses coroutine and yield null for confirmation dialog.
        /// </summary>
        /// <param name="dialog"> </param>
        /// <returns>
        /// null. Yield return null is here to await enum change for confirmation popup. Once an
        /// option is selected (Yes or No) the variable dialog value changes, completing the dialog.
        /// </returns>
        public IEnumerator DeleteCoroutine(ConfirmationDialog dialog)
        {
            var fileName = OnClickSaveFile.FileName;
            fileHandling = Factory.CreateFileHandling();

            if (File.Exists($@"{Application.persistentDataPath}\saves\{fileName}.sav"))
            {
                while (dialog.result == DialogResults.None)
                {
                    yield return null;
                }
                if (dialog.result == DialogResults.Yes)
                {
                    fileHandling.Delete(fileName);
                    var GO = GameObject.Find($"{fileName}");
                    Destroy(GO);
                    HideDialog();
                }
                else
                {
                    HideDialog();
                }
                dialog.result = DialogResults.None;
            }
            var onEnable = transform.GetComponentInParent<SetOnEnable>();
            onEnable.OnEnable();
            StopCoroutine(DeleteCoroutine(dialog));
        }

        /// <summary> OnClick for the Delete button in save/load file menus. </summary>
        public void OnDelete()
        {
            var fileName = OnClickSaveFile.FileName;
            if (File.Exists($@"{Application.persistentDataPath}\saves\{fileName}.sav"))
            {
                ConfirmationDialog dialog = ShowDialog();
                StartCoroutine(DeleteCoroutine(dialog));
            }
            else
            {
                var dialog = transform.Find("FileNotFound").gameObject;
                dialog.SetActive(true);
            }
        }

        private void HideDialog()
        {
            var confirmDialog = GameObject.Find("DeleteConfirmation");
            confirmDialog.SetActive(false);
        }

        private ConfirmationDialog ShowDialog()
        {
            var DeleteConfParent = GameObject.Find("Delete");
            var confirmDialog = DeleteConfParent.transform.Find("DeleteConfirmation").gameObject;
            confirmDialog.transform.Find("DeleteText").GetComponent<Text>().text
                = $"Are you sure you want to delete this file?\n{OnClickSaveFile.FileName}";
            confirmDialog.SetActive(true);
            return confirmDialog.GetComponent<ConfirmationDialog>();
        }
    }
}
