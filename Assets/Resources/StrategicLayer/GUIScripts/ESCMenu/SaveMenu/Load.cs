﻿using AlienOccupation.Assets.Resources.Global;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary> Loads game state when triggered (usually by clicking Load) </summary>
    public class Load : MonoBehaviour
    {
        private IFileHandling FileHandling;

        /// <summary> Coroutine to handle load confirmation and game state loading. </summary>
        /// <returns>
        /// null. Yield return null is here to await enum change for confirmation popup. Once an
        /// option is selected (Yes or No) the variable dialog value changes, completing the dialog.
        /// </returns>
        public IEnumerator LoadCoroutine()
        {
            FileHandling = Factory.CreateFileHandling();

            var fileName = OnClickSaveFile.FileName;

            if (File.Exists($@"{Application.persistentDataPath}\saves\{fileName}.sav"))
            {
                ConfirmationDialog dialog = ShowDialog();
                while (dialog.result == DialogResults.None)
                {
                    yield return null;
                }
                if (dialog.result == DialogResults.Yes)
                {
                    if (SceneManager.GetActiveScene().name == "GameStartMenu")
                    {
                        FileHandling.Load(fileName);
                        GameData.GDInstance.GlobalVariables.IsNewGame = false;
                        var asyncLoad = SceneManager.LoadSceneAsync("StrategicLayer");

                        while (!asyncLoad.isDone)
                        {
                            yield return null;
                        }
                    }
                    else { FileHandling.Load(fileName); }

                    HideDialog();
                }
                else
                {
                    HideDialog();
                }
                dialog.result = DialogResults.None;
            }
        }

        /// <summary>
        /// OnClick to check if file exists, should file state have changed between opening menu and
        /// clicking file to load.
        /// </summary>
        public void OnLoadClick()
        {
            string fileName = OnClickSaveFile.FileName;
            if (!File.Exists($@"{Application.persistentDataPath}\saves\{fileName}.sav"))
            {
                var dialog = transform.Find("FileNotFound").gameObject;
                dialog.SetActive(true);
            }
            else
            {
                StartCoroutine(LoadCoroutine());
            }
        }

        private void HideDialog()
        {
            var confirmDialog = GameObject.Find("LoadConfirmation");
            confirmDialog.SetActive(false);
        }

        private ConfirmationDialog ShowDialog()
        {
            var LoadConfParent = GameObject.Find("Load");
            var confirmDialog = LoadConfParent.transform.Find("LoadConfirmation").gameObject;
            confirmDialog.transform.Find("Text").GetComponent<Text>().text
                = $"Are you sure you want to Load this file?\n{OnClickSaveFile.FileName}";
            confirmDialog.SetActive(true);
            return confirmDialog.GetComponent<ConfirmationDialog>();
        }
    }
}
