﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl
{
    public interface ISpeedController
    {
        void ChangeSpeed(GameSpeed speed);
    }
}