﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl
{
    /// <summary> Enum for deciding game speed. </summary>
    public enum GameSpeed
    {
        Pause,
        Play,
        Fast,
        Fastest
    }

    /// <summary> Controls the game speed and game speed buttons. </summary>
    public class SpeedController : ISpeedController
    {
        /// <summary>
        /// Changes game speed baserd on enum passed via OnClick event for respective buttons.
        /// </summary>
        /// <param name="speed"> </param>
        public void ChangeSpeed(GameSpeed speed)
        {
            // Loads speed button game objects into a dictionary for switching "pressed" colors.
            var buttons = new Dictionary<string, GameObject>()
            {
                {"Pause", GameObject.Find("Pause") },
                {"Play", GameObject.Find("Play") },
                {"Fast", GameObject.Find("Fast") },
                {"Fastest", GameObject.Find("Fastest") }
            };

            switch (speed)
            {
                case GameSpeed.Play:
                    {
                        Time.timeScale = 1F;
                        foreach (var button in buttons)
                        {
                            if (button.Key == "Play") { button.Value.GetComponent<Image>().color = Color.blue; }
                            else { button.Value.GetComponent<Image>().color = Color.white; }
                        }
                        break;
                    }
                case GameSpeed.Fast:
                    {
                        Time.timeScale = 2F;
                        foreach (var button in buttons)
                        {
                            if (button.Key == "Fast") { button.Value.GetComponent<Image>().color = Color.blue; }
                            else { button.Value.GetComponent<Image>().color = Color.white; }
                        }
                        break;
                    }
                case GameSpeed.Fastest:
                    {
                        Time.timeScale = 4F;
                        foreach (var button in buttons)
                        {
                            if (button.Key == "Fastest") { button.Value.GetComponent<Image>().color = Color.blue; }
                            else { button.Value.GetComponent<Image>().color = Color.white; }
                        }
                        break;
                    }
                case GameSpeed.Pause:
                    {
                        if (Time.timeScale == 0)
                        {
                            Time.timeScale = 1;
                            buttons["Pause"].GetComponent<Image>().color = Color.white;
                            buttons["Play"].GetComponent<Image>().color = Color.blue;
                        }
                        else
                        {
                            Time.timeScale = 0;
                            foreach (var button in buttons)
                            {
                                if (button.Key == "Pause") { button.Value.GetComponent<Image>().color = Color.blue; }
                                else { button.Value.GetComponent<Image>().color = Color.white; }
                            }
                        }

                        break;
                    }
            }
        }
    }
}
