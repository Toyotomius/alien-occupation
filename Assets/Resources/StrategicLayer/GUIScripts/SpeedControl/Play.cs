﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl
{
    public class Play : MonoBehaviour, ISpeed
    {
        private ISpeedController speedController;

        /// <summary> OnClick script for Play button. Sets speed to standard game speed. </summary>
        public void SetSpeed()
        {
            speedController = Factory.CreateSpeedContoller();
            speedController.ChangeSpeed(GameSpeed.Play);
        }
    }
}
