﻿using UnityEngine;
using AlienOccupation.Assets.Resources.Global;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl
{
    public class Fast : MonoBehaviour, ISpeed
    {
        private ISpeedController speedController;

        /// <summary>
        /// OnClick script for Fast(&gt;&gt;) button. Script for changing game speed to "fast"
        /// </summary>
        public void SetSpeed()
        {
            speedController = Factory.CreateSpeedContoller();
            speedController.ChangeSpeed(GameSpeed.Fast);
        }
    }
}
