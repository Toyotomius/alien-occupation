﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl
{
    public class Fastest : MonoBehaviour, ISpeed
    {
        private ISpeedController speedController;

        /// <summary>
        /// OnClick script for Fastest(&gt;&gt;&gt;) button. Script for changing game speed to "fastest"
        /// </summary>
        public void SetSpeed()
        {
            speedController = Factory.CreateSpeedContoller();
            speedController.ChangeSpeed(GameSpeed.Fastest);
        }
    }
}
