﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl
{
    public class Pause : MonoBehaviour, ISpeed
    {
        private ISpeedController speedController;

        /// <summary>
        /// OnClick script for Pause button. Pauses game by setting global speed to 0.
        /// </summary>
        public void SetSpeed()
        {
            speedController = Factory.CreateSpeedContoller();
            speedController.ChangeSpeed(GameSpeed.Pause);
        }
    }
}
