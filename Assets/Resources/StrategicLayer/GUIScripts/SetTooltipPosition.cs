﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// Script for positioning tooltips at the mouse position.
    /// </summary>
    public class SetTooltipPosition : MonoBehaviour
    {
        public Vector3 Adjustment;
        public void LateUpdate()
        {
            transform.position = (Input.mousePosition + new Vector3(5, 0, 0) + Adjustment);
        }
        public void OnEnable()
        {
            this.transform.SetAsLastSibling();
        }
    }
}
