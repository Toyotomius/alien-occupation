﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;
using static AlienOccupation.Assets.Resources.Global.GameData;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.PlayerResources.Tooltips
{
    public class FavorTooltip : MonoBehaviour, ITooltipControls
    {
        private IShowHide showHideGameObject;

        public void Awake()
        {
            showHideGameObject = Factory.CreateShowHide();
        }

        public void HideGameObject()
        {
            showHideGameObject.HideGameObject(Tooltip.Inst.UITooltip);
        }

        public void ShowGameObject()
        {
            showHideGameObject.ShowGameObject(Tooltip.Inst.UITooltip, GDInstance.PlayerResources["Favor"].TooltipText);
        }
    }
}

// TODO: These are pretty inefficient. Replace it at some point. No need for a file for each resource probably.
