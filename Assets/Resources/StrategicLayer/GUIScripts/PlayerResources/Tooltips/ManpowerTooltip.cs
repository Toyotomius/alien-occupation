﻿using AlienOccupation.Assets.Resources;
using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.PlayerResources.Tooltips;
using UnityEngine;
using static AlienOccupation.Assets.Resources.Global.GameData;

public class ManpowerTooltip : MonoBehaviour, ITooltipControls
{
    private IShowHide showHideGameObject;

    public void Awake()
    {
        showHideGameObject = Factory.CreateShowHide();
    }

    public void HideGameObject()
    {
        showHideGameObject.HideGameObject(Tooltip.Inst.UITooltip);
    }

    public void ShowGameObject()
    {
        showHideGameObject.ShowGameObject(Tooltip.Inst.UITooltip, GDInstance.PlayerResources["Manpower"].TooltipText);
    }
}
