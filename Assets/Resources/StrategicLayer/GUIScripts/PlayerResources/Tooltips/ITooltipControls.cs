﻿namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.PlayerResources.Tooltips
{
    public interface ITooltipControls
    {
        void HideGameObject();

        void ShowGameObject();
    }
}
