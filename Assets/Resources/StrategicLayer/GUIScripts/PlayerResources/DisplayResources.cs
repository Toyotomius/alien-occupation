﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.FormatNum;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.PlayerResources
{
    public class DisplayResources : MonoBehaviour

    {
        /// <summary> Updates displayed amounts and requirements for each resource. </summary>
        public void LateUpdate()
        {
            foreach (var resource in GameData.GDInstance.PlayerResources)
            {
                var resourcePrefab = gameObject.transform.Find(resource.Name);
                var resourceAmount = resourcePrefab.Find("ResourceQuantity");
                var text = resourceAmount.GetComponent<Text>();
                text.text = resource.Required <= 0 ? $"{FormatNumber(resource.Count)}" :
                    $"{FormatNumber(resource.Count)}\n{FormatNumber(resource.Required)}";
            }
        }

        /// <summary> Instantiates the resource prefabs and assigns them to the parent UI object. </summary>
        public void Start()
        {
            var resourceBar = GameObject.Find("ResourceBar").transform;
            foreach (var resource in GameData.GDInstance.PlayerResources)
            {
                var resourcePrefab = (GameObject)Instantiate(UnityEngine.Resources.Load("StrategicLayer/GUIPrefab/ResourceName", typeof(GameObject)));
                resourcePrefab.transform.SetParent(resourceBar);
                resourcePrefab.name = resource.Name;

                var resourceIconGO = resourcePrefab.transform.Find("ResourceIcon");
                var resourceIcon = resourceIconGO.GetComponent<Image>();
                resourceIcon.sprite = (Sprite)UnityEngine.Resources.Load(resource.SpriteLocation, typeof(Sprite));
            }
        }
    }
}

// TODO: Show rate of change.
