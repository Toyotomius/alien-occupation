﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    public class TechMenuToggle : MonoBehaviour
    {
        private bool IsOpen = false;

        private IShowHide ShowHide;
        private GameObject TechMenu;

        public void MenuToggle()
        {
            IsOpen = TechMenu.activeSelf;

            if (!IsOpen)
            {
                ShowHide.ShowGameObject(TechMenu);
            }
            else
            {
                ShowHide.HideGameObject(TechMenu);
            }
        }

        public void Start()
        {
            TechMenu = GameObject.Find("StrategicLayerCanvas").transform.Find("TechBackground").gameObject;
            ShowHide = Factory.CreateShowHide();
        }
    }
}
