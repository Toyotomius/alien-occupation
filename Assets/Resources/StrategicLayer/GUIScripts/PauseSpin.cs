﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts
{
    /// <summary>
    /// Script for pausing the spin of the earth
    /// </summary>
    public class PauseSpin : MonoBehaviour
    {
        public void TogglePause()
        {
            GameObject earth = GameObject.Find("Earth");
            var spin = earth.GetComponent<SpinFree>();
            if (spin.spin) { spin.spin = false; }
            else { spin.spin = true; }
        }
    }
}
