﻿using AlienOccupation.Assets.Resources.Global;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.GameStartMenu
{
    public class ClickNewGame : MonoBehaviour
    {
        public void OnClickNewGame()
        {
            GameData.GDInstance.GlobalVariables.IsNewGame = true;
            var loadStrategicLayer = Factory.CreateLoadScene();
            loadStrategicLayer.SceneLoad(Constants.StrategicLayerIndex);
        }
    }
}
