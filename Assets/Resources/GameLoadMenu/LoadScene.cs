﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary> Loads strategic layer when New Game clicked in GameStartMenu Scene (scene 0). </summary>
namespace AlienOccupation.Assets.Resources
{
    public class LoadScene : ILoadScene
    {
        public void SceneLoad(int buildIndex)
        {
            SceneManager.LoadScene(buildIndex);
        }
        public void SceneLoad(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
