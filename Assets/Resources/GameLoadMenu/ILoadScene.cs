﻿namespace AlienOccupation.Assets.Resources
{
    public interface ILoadScene
    {
        void SceneLoad(int buildIndex);
        void SceneLoad(string sceneName);
    }
}