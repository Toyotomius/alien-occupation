﻿using AlienOccupation.Assets.Resources.Global;
using AlienOccupation.Assets.Resources.StrategicLayer;
using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using AlienOccupation.Assets.Resources.StrategicLayer.EventManager;
using AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents;
using AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts;
using AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts.Interfaces;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts.SpeedControl;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents;
using AlienOccupation.Assets.Resources.StrategicLayer.Regions;

/// <summary> Main factory class for managing instantiations as needed. </summary>
namespace AlienOccupation.Assets.Resources.Global
{
    public static class Factory
    {
        public static ILoadScene CreateLoadScene()
        {
            return new LoadScene();
        }
        public static ILoadVariableNames CreateLoadVariableNames()
        {
            return new LoadVariableNames();
        }
        public static IBuildingManager CreateBuildingManager()
        {
            return new BuildingManager();
        }
        public static IApplyEventEffects CreateApplyEventAffects()
        {
            return new ApplyEventEffects();
        }

        public static IFileHandling CreateFileHandling()
        {
            return new FileHandling();
        }

        public static IGameEventHandler CreateGameEventHandler()
        {
            return GameEventHandler.GameEventHandlerInstance;
        }

        public static IIsPercentChanceTriggered CreateIsPercentChanceTriggered()
        {
            return new IsPercentChanceTriggered();
        }

        public static IPlayerNotification CreatePlayerNotification()
        {
            return PlayerNotification.PlayerNotificationInstance;
        }

        public static IRegion CreateRegion(bool SetDefaults)
        {
            return new Region(CreateSubRegion(SetDefaults), CreateSubRegion(SetDefaults), CreateSubRegion(SetDefaults));
        }

        public static IRevoltManager CreateRevoltManager()
        {
            return new RevoltManager();
        }

        public static IShowHide CreateShowHide()
        {
            return new ShowHide();
        }

        public static ISpeedController CreateSpeedContoller()
        {
            return new SpeedController();
        }

        public static ISubRegion CreateSubRegion(bool SetDefaults)
        {
            return new SubRegion(SetDefaults);
        }
    }
}
