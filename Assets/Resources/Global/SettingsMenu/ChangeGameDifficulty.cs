﻿using AlienOccupation.Assets.Resources.Global.Extensions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static AlienOccupation.Assets.Resources.Global.GameData;

namespace AlienOccupation.Assets.Resources.Global.SettingsMenu
{
    public class ChangeGameDifficulty : MonoBehaviour
    {
        public Slider AttackModifierSlider;
        public TMP_Text AttackModifierValueText;
        public TMP_Dropdown DifficultyPresetsDropdown;
        public Slider FavorAdjustmentSlider;
        public TMP_Text FavorAdjustmentValueText;
        public Slider HappinessModifierSlider;
        public TMP_Text HappinessModifierValueText;
        public Slider ProductivityModifierSlider;
        public TMP_Text ProductivityModifierValueText;
        public Slider QuotaRequirementModifierSlider;
        public TMP_Text QuotaRequirementModifierValueText;
        public Slider ResourceRateModifierSlider;
        public TMP_Text ResourceRateModifierValueText;
        public Slider RevoltRiskSlider;
        public TMP_Text RevoltRiskValueText;
        public Slider RevoltSeveritySlider;
        public TMP_Text RevoltSeverityValueText;
        private readonly List<string> DifficultyPresetNames = new() { "Easy", "Normal", "Hard", "Very Hard", "Insane" };
        private Slider[] Sliders;

        /// <summary> Sets all the difficulty changes to GameData singleton for save file. </summary>
        public void Apply()
        {
            GDInstance.GlobalVariables.Difficulty.AttackModifier = AttackModifierSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.FavorAdjustment = FavorAdjustmentSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.HappinessModifier = HappinessModifierSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.ProductivityModifier = ProductivityModifierSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.QuotaRequirementModifier = QuotaRequirementModifierSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.ResourceRateModifier = ResourceRateModifierSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.RevoltRisk = RevoltRiskSlider.value.RoundToTen();
            GDInstance.GlobalVariables.Difficulty.RevoltSeverity = RevoltSeveritySlider.value.RoundToTen();
        }

        /// <summary> Adds preset options to dropdown and creates slider array for later use </summary>
        public void Awake()
        {
            DifficultyPresetsDropdown.ClearOptions();
            DifficultyPresetsDropdown.AddOptions(DifficultyPresetNames);
            DifficultyPresetsDropdown.RefreshShownValue();

            Sliders = new Slider[]
            {
                AttackModifierSlider, FavorAdjustmentSlider, HappinessModifierSlider,
                ProductivityModifierSlider,QuotaRequirementModifierSlider,
                ResourceRateModifierSlider,RevoltRiskSlider,RevoltSeveritySlider
            };
        }

        /// <summary> Reverts settings to last saved modifiers </summary>
        public void Cancel()
        {
            AttackModifierSlider.value = GDInstance.GlobalVariables.Difficulty.AttackModifier;
            FavorAdjustmentSlider.value = GDInstance.GlobalVariables.Difficulty.FavorAdjustment;
            HappinessModifierSlider.value = GDInstance.GlobalVariables.Difficulty.HappinessModifier;
            ProductivityModifierSlider.value = GDInstance.GlobalVariables.Difficulty.ProductivityModifier;
            QuotaRequirementModifierSlider.value = GDInstance.GlobalVariables.Difficulty.QuotaRequirementModifier;
            ResourceRateModifierSlider.value = GDInstance.GlobalVariables.Difficulty.ResourceRateModifier;
            RevoltRiskSlider.value = GDInstance.GlobalVariables.Difficulty.RevoltRisk;
            RevoltSeveritySlider.value = GDInstance.GlobalVariables.Difficulty.RevoltSeverity;
        }

        public void ChangeAttackModifier(float value) => AttackModifierValueText.text = value.RoundToTen().ToString();

        public void ChangeFavorAdjustment(float value) => FavorAdjustmentValueText.text = value.RoundToTen().ToString();

        public void ChangeHappinessModifier(float value) => HappinessModifierValueText.text = value.RoundToTen().ToString();

        public void ChangePreset(int dropdownIndex)
        {
            for (int i = 0; i < Sliders.Length; i++)
            {
                Sliders[i].value = (float)Constants.ConstantsJObject["DifficultyPresetValues"][dropdownIndex][i];
            }
        }

        public void ChangeProductivityModifier(float value) => ProductivityModifierValueText.text = value.RoundToTen().ToString();

        public void ChangeQuotaRequirements(float value) => QuotaRequirementModifierValueText.text = value.RoundToTen().ToString();

        public void ChangeResourceRateModifier(float value) => ResourceRateModifierValueText.text = value.RoundToTen().ToString();

        public void ChangeRevoltRisk(float value) => RevoltRiskValueText.text = value.RoundToTen().ToString();

        public void ChangeRevoltSeverity(float value) => RevoltSeverityValueText.text = value.RoundToTen().ToString();

        public void OnEnable()
        {
            // Displays Slider value from last saved adjustments
            AttackModifierSlider.value = GDInstance.GlobalVariables.Difficulty.AttackModifier;
            FavorAdjustmentSlider.value = GDInstance.GlobalVariables.Difficulty.FavorAdjustment;
            HappinessModifierSlider.value = GDInstance.GlobalVariables.Difficulty.HappinessModifier;
            ProductivityModifierSlider.value = GDInstance.GlobalVariables.Difficulty.ProductivityModifier;
            QuotaRequirementModifierSlider.value = GDInstance.GlobalVariables.Difficulty.QuotaRequirementModifier;
            ResourceRateModifierSlider.value = GDInstance.GlobalVariables.Difficulty.ResourceRateModifier;
            RevoltRiskSlider.value = GDInstance.GlobalVariables.Difficulty.RevoltRisk;
            RevoltSeveritySlider.value = GDInstance.GlobalVariables.Difficulty.RevoltSeverity;

            // Displays Modifier text from slider
            AttackModifierValueText.text = AttackModifierSlider.value.RoundToTen().ToString();
            FavorAdjustmentValueText.text = FavorAdjustmentSlider.value.RoundToTen().ToString();
            HappinessModifierValueText.text = HappinessModifierSlider.value.RoundToTen().ToString();
            ProductivityModifierValueText.text = ProductivityModifierSlider.value.RoundToTen().ToString();
            QuotaRequirementModifierValueText.text = QuotaRequirementModifierSlider.value.RoundToTen().ToString();
            ResourceRateModifierValueText.text = ResourceRateModifierSlider.value.RoundToTen().ToString();
            RevoltRiskValueText.text = RevoltRiskSlider.value.RoundToTen().ToString();
            RevoltSeverityValueText.text = RevoltSeveritySlider.value.RoundToTen().ToString();

            // Compares slider values to presets and changes the dropdown index to correct preset if matched.
            JArray presetValuesArrays = (JArray)Constants.ConstantsJObject["DifficultyPresetValues"];
            for (var x = 0; x < presetValuesArrays.Count; x++)
            {
                var count = 0;
                for (int i = 0; i < Sliders.Length; i++)
                {
                    if (Sliders[i].value != (float)presetValuesArrays[x][i]) { break; }
                    count++;
                }
                if (count == Sliders.Length) { DifficultyPresetsDropdown.value = x; }
            }
        }
    }
}
