﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global.SettingsMenu
{
    public class LoadProgramSettingsOnLaunch : MonoBehaviour
    {
        public void Start()
        {
            // Set constants as soon as game loads to the Main Menu. May change if I have an intro
            // video or something.
            Constants.SetMainMenuConstants();

            // Loads the settings for video/audio
            ChangeProgramSettings loadSettings = this.transform.parent.Find("SubMenuBackground/SettingsMenu").GetComponent<ChangeProgramSettings>();
            loadSettings.LoadSettings();
        }
    }
}
