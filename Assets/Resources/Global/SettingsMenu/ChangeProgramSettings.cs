﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace AlienOccupation.Assets.Resources.Global.SettingsMenu
{
    /// <summary>
    /// Class for changing and saving all program related settings, such as video and audio.
    /// </summary>
    public class ChangeProgramSettings : MonoBehaviour
    {
        public AudioMixer AudioMixer;
        public Toggle FullscreenToggle;
        public Slider MasterVolumeSlider;
        public Slider MusicVolumeSlider;
        public TMP_Dropdown QualityDropdown;
        public TMP_Dropdown ResolutionDropdown;
        public Slider SoundEffectsVolumeSlider;
        private readonly SettingsProperties Settings = new();
        private int CurrentResolutionIndex;
        private Resolution[] Resolutions;

        /// <summary>
        /// Saves values into backing static fields (for scene switching) and writes settings to
        /// perference file
        /// </summary>
        public void Apply()
        {
            Settings.CurrentMasterVolume = MasterVolumeSlider.value;
            Settings.CurrentMusicVolume = MusicVolumeSlider.value;
            Settings.CurrentSoundEffectsVolume = SoundEffectsVolumeSlider.value;
            Settings.IsFullscreen = Screen.fullScreen;
            Settings.QualityIndex = QualityDropdown.value;

            //Settings.CurrentResolution = Screen.currentResolution;
            CurrentResolutionIndex = ResolutionDropdown.value;
            var json = JsonConvert.SerializeObject(Settings);
            using (StreamWriter sw = new($@"{Application.streamingAssetsPath}\GamePrefs.ini", false))
            {
                sw.Write(json);
            }
        }

        /// <summary>
        /// Populates all the drop down options and sets current index that matches existing setting
        /// </summary>
        public void Awake()
        {
            ResolutionDropdown.ClearOptions();
            List<string> resOptions = new();
            Resolutions = Screen.resolutions;

            for (int i = 0; i < Resolutions.Length; i++)
            {
                string option = Resolutions[i].width + " x " +
                         Resolutions[i].height + $" ({Resolutions[i].refreshRate} hz)";
                resOptions.Add(option);
                if (Resolutions[i].width == Screen.currentResolution.width
                      && Resolutions[i].height == Screen.currentResolution.height)
                    CurrentResolutionIndex = i;
            }
            ResolutionDropdown.AddOptions(resOptions);
            ResolutionDropdown.RefreshShownValue();
            ResolutionDropdown.value = CurrentResolutionIndex;

            QualityDropdown.ClearOptions();
            List<string> qualityOptions = new();
            int currentQualityIndex = 0;
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                qualityOptions.Add(QualitySettings.names[i]);
                if (i == QualitySettings.GetQualityLevel())
                {
                    currentQualityIndex = i;
                }
            }
            QualityDropdown.AddOptions(qualityOptions);
            QualityDropdown.value = currentQualityIndex;
            QualityDropdown.RefreshShownValue();
        }

        /// <summary> Reverts values to last saved when cancel button is clicked </summary>
        public void CancelSettings()
        {
            MasterVolumeSlider.value = Settings.CurrentMasterVolume;
            MusicVolumeSlider.value = Settings.CurrentMusicVolume;
            SoundEffectsVolumeSlider.value = Settings.CurrentSoundEffectsVolume;

            QualityDropdown.value = Settings.QualityIndex;
            SetResolution(CurrentResolutionIndex);
            FullscreenToggle.isOn = Settings.IsFullscreen; // Must be below resolution change. Don't ask me why. Unity I guess.
        }

        /// <summary> Loads all the program settings as needed </summary>
        public void LoadSettings()
        {
            string prefsJson;
            using (StreamReader sr = new($@"{Application.streamingAssetsPath}\GamePrefs.ini"))
            {
                prefsJson = sr.ReadToEnd();
            }
            JsonConvert.DeserializeObject<SettingsProperties>(prefsJson);
            SetMainVolume(Settings.CurrentMasterVolume);
            SetSoundEffectsVolume(Settings.CurrentSoundEffectsVolume);
            SetMusicVolume(Settings.CurrentMusicVolume);
            SetFullscreen(Settings.IsFullscreen);
            SetQuality(Settings.QualityIndex);
        }

        /// <summary>
        /// Sets current values in the menu if someone changed the value then hit cancel or changed
        /// it from a different scene.
        /// </summary>
        public void OnEnable()
        {
            MasterVolumeSlider.value = Settings.CurrentMasterVolume;
            Debug.Log($"{MasterVolumeSlider.name} set to {Settings.CurrentMasterVolume}");
            MusicVolumeSlider.value = Settings.CurrentMusicVolume;
            Debug.Log($"{MusicVolumeSlider.name} set to {Settings.CurrentMusicVolume}");
            SoundEffectsVolumeSlider.value = Settings.CurrentSoundEffectsVolume;
            Debug.Log($"{SoundEffectsVolumeSlider.name} set to {Settings.CurrentSoundEffectsVolume}");
            ResolutionDropdown.value = CurrentResolutionIndex;
            FullscreenToggle.isOn = Settings.IsFullscreen;
        }

        public void SetFullscreen(bool isFullscreen) => Screen.fullScreen = isFullscreen;

        public void SetMainVolume(float volume) => AudioMixer.SetFloat("Master", volume);

        public void SetMusicVolume(float volume) => AudioMixer.SetFloat("Music", volume);

        public void SetQuality(int qualityIndex) => QualitySettings.SetQualityLevel(qualityIndex);

        public void SetResolution(int resolutionIndex)
        {
            Resolution resolution = Resolutions[resolutionIndex];
            Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        }

        public void SetSoundEffectsVolume(float volume) => AudioMixer.SetFloat("SoundEffects", volume);
    }

    // TODO: Remember to change the defaults in the ini.

    internal static class ProgramSettings
    {
        internal static float CurrentMasterVolume;

        internal static float CurrentMusicVolume;

        //internal static Resolution CurrentResolution;

        internal static float CurrentSoundEffectsVolume;

        internal static bool IsFullscreen;

        internal static int QualityIndex;
    }

    /// <summary> Made to encapsulate the statically stored values for serialization </summary>
    internal class SettingsProperties
    {
        [JsonRequired]
        internal float CurrentMasterVolume
        {
            get { return ProgramSettings.CurrentMasterVolume; }
            set { ProgramSettings.CurrentMasterVolume = value; }
        }

        [JsonRequired]
        internal float CurrentMusicVolume
        {
            get { return ProgramSettings.CurrentMusicVolume; }
            set { ProgramSettings.CurrentMusicVolume = value; }
        }

        //[JsonRequired]
        //internal Resolution CurrentResolution
        //{
        //    get { return ProgramSettings.CurrentResolution; }
        //    set { ProgramSettings.CurrentResolution = value; }
        //}

        [JsonRequired]
        internal float CurrentSoundEffectsVolume
        {
            get { return ProgramSettings.CurrentSoundEffectsVolume; }
            set { ProgramSettings.CurrentSoundEffectsVolume = value; }
        }

        [JsonRequired]
        internal bool IsFullscreen
        {
            get { return ProgramSettings.IsFullscreen; }
            set { ProgramSettings.IsFullscreen = value; }
        }

        [JsonRequired]
        internal int QualityIndex
        {
            get { return ProgramSettings.QualityIndex; }
            set { ProgramSettings.QualityIndex = value; }
        }
    }
}
