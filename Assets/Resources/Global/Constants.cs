﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global
{
    public static class Constants
    {
        public static GameObject BuildingCategoryParentPrefab;
        public static GameObject BuildingParent;
        public static GameObject BuildingPrefab;
        public static GameObject BuildingSlot;
        public static GameObject BuildingTierButton;
        public static GameObject BuildingTooltipPrefab;
        public static GameObject BuildListSlotPrefab;
        public static JObject ConstantsJObject;
        public static int DefaultPenaltyResourceDeduction;
        public static Sprite EmptyBuilingSprite;
        public static GameObject EventFlavorParentPrefab;
        public static GameObject EventRegionPrefab;
        public static int GameOverIndex;
        public static int GameStartMenuIndex;
        public static Sprite HarvestBuildingTestSprite;
        public static GameObject RegionOverlay;
        public static JObject RevoltSettings;
        public static DateTime StartDate;
        public static GameObject StrategicLayerCanvas;
        public static int StrategicLayerIndex;
        public static GameObject TechCardPrefab;
        public static GameObject TechCategoryPrefab;
        public static GameObject TechGroupParent;
        public static GameObject TechTabPrefab;
        public static GameObject TechTooltipPrefab;
        
        public static GameObject TooltipPrefab;


        public static void SetMainMenuConstants()
        {
            using (var sr = new StreamReader($@"{Application.streamingAssetsPath}\Constants.cfg"))
            {
                var constantsJson = sr.ReadToEnd();
                ConstantsJObject = JObject.Parse(constantsJson);
            }

            TooltipPrefab =
                        (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TooltipPrefab"], typeof(GameObject));
            GameOverIndex = (int)ConstantsJObject["ConstantValues"]["GameOverIndex"];
            GameStartMenuIndex = (int)ConstantsJObject["ConstantValues"]["GameStartMenuIndex"];
            StrategicLayerIndex = (int)ConstantsJObject["ConstantValues"]["StrategicLayerIndex"];
        }
        //TODO: Move the below to a cfg.
        public static void SetConstants()
        {
            //using (var sr = new StreamReader($@"{Application.streamingAssetsPath}\Constants.cfg"))
            //{
            //    var constantsJson = sr.ReadToEnd();
            //    ConstantsJObject = JObject.Parse(constantsJson);
            //}

            StrategicLayerCanvas = GameObject.Find("StrategicLayerCanvas");
            RegionOverlay = StrategicLayerCanvas.transform.Find("RegionOverlayBackground").gameObject;

            EmptyBuilingSprite =
                        (Sprite)UnityEngine.Resources.Load("StrategicLayer/Sprites/EmptyBuilding", typeof(Sprite));
            EventFlavorParentPrefab =
                (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["EventFlavorParentPrefab"], typeof(GameObject));
            EventRegionPrefab =
                (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["EventRegionPrefab"], typeof(GameObject));

            HarvestBuildingTestSprite =
                (Sprite)UnityEngine.Resources.Load("StrategicLayer/Sprites/BuildingPlaceholder", typeof(Sprite));

            StartDate = DateTime.Parse((string)ConstantsJObject["ConstantValues"]["StartDate"]);

            TooltipPrefab =
                        (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TooltipPrefab"], typeof(GameObject));

            TechTooltipPrefab =
                        (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TechTooltipPrefab"], typeof(GameObject));
            TechTabPrefab = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TechTabPrefab"], typeof(GameObject));
            TechCategoryPrefab = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TechCategoryPrefab"], typeof(GameObject));
            TechGroupParent = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TechGroupParent"], typeof(GameObject));
            TechCardPrefab = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["TechCardPrefab"], typeof(GameObject));

            BuildingSlot = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildingSlot"], typeof(GameObject));
            BuildListSlotPrefab = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildListSlotPrefab"], typeof(GameObject));
            BuildingPrefab = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildingPrefab"], typeof(GameObject));
            BuildingCategoryParentPrefab =
                (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildingCategoryParentPrefab"], typeof(GameObject));
            BuildingParent = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildingParent"], typeof(GameObject));
            BuildingTierButton = (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildingTierButton"], typeof(GameObject));
            BuildingTooltipPrefab =
                (GameObject)UnityEngine.Resources.Load((string)ConstantsJObject["PrefabLocations"]["BuildingTooltipPrefab"], typeof(GameObject));

            DefaultPenaltyResourceDeduction = (int)ConstantsJObject["ConstantValues"]["DefaultPenaltyResourceDeduction"];
            GameOverIndex = (int)ConstantsJObject["ConstantValues"]["GameOverIndex"];
            GameStartMenuIndex = (int)ConstantsJObject["ConstantValues"]["GameStartMenuIndex"];
            StrategicLayerIndex = (int)ConstantsJObject["ConstantValues"]["StrategicLayerIndex"];

            using (var sr = new StreamReader(@$"{Application.streamingAssetsPath}\RevoltSettings.json"))
            {
                var jsonStr = sr.ReadToEnd();
                RevoltSettings = JObject.Parse(jsonStr);
            }
        }
    }
}
