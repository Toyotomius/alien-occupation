﻿using AlienOccupation.Assets.Resources.StrategicLayer;
using AlienOccupation.Assets.Resources.StrategicLayer.Tech;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary>
    /// Singleton to handle all important game state information. Done this way for easy json serialization.
    /// </summary>
    public sealed class GameData
    {
        static GameData()
        {
        }

        private GameData()
        {
            GlobalVariables = new GlobalVariables();

            PlayerResources = new PlayerResources();

            Regions = new WorldRegions(false);
            Technology = new Technology();
        }

        public static GameData GDInstance { get; internal set; } = new GameData();

        public GlobalVariables GlobalVariables { get; set; }

        public PlayerResources PlayerResources { get; set; }

        public WorldRegions Regions { get; set; }

        public Technology Technology { get; set; }
    }
}
