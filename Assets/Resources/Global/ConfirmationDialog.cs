﻿using UnityEngine;
/// <summary>
/// Script for managing confirmation dialog enums. Used as part of confirmation dialog popups.
/// </summary>
namespace AlienOccupation.Assets.Resources.Global
{
    public enum DialogResults
    {
        None, Yes, No
    }

    public class ConfirmationDialog : MonoBehaviour
    {
        internal DialogResults result = DialogResults.None;
        public void OnYes()
        {
            result = DialogResults.Yes;
        }
        public void OnNo()
        {
            result = DialogResults.No;
        }
    }
}
