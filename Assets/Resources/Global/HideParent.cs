﻿using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary>
    /// Used to hide parent GameObject that script is attached to. Called by various GameObjects to close (deactivate)
    /// menus. Most Close/Exit/Cancel buttons call this script.
    /// </summary>
    public class HideParent : MonoBehaviour
    {
        private IShowHide ShowHide;

        public void Hide()
        {
            var parent = this.transform.parent.gameObject;
            ShowHide = Factory.CreateShowHide();

            ShowHide.HideGameObject(parent);
        }
    }
}
