﻿using Newtonsoft.Json.Linq;
using System.IO;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global
{
    public static class VariableNames
    {
        public static dynamic Names;
    }

    public class LoadVariableNames : ILoadVariableNames
    {
        public void LoadDynamicResource()
        {
            string nameResourceJson;
            using (var sr = new StreamReader($@"{Application.streamingAssetsPath}\VariableNames.cfg"))
            {
                nameResourceJson = sr.ReadToEnd();
            }
            VariableNames.Names = JObject.Parse(nameResourceJson);
        }
    }
}
