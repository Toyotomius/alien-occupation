﻿namespace AlienOccupation.Assets.Resources.Global
{
    public static class FormatNum
    {
        public static string FormatNumber(long num)
        {
            if (num >= 100000000)
            {
                return (num / 1000000D).ToString("0.#M");
            }
            if (num >= 1000000)
            {
                return (num / 1000000D).ToString("0.##M");
            }
            if (num >= 100000)
            {
                return (num / 1000D).ToString("0k");
            }
            if (num >= 10000)
            {
                return (num / 1000D).ToString("0.00k");
            }

            return num.ToString("#,0");
        }
    }
}
