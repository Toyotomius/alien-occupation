﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlienOccupation.Assets.Resources.Global
{
    public static class ParseRegionName
    {
        public static string Parse(string RegionName) =>
            RegionName switch
            {
                "NorthAmerica" => "North America",
                "SouthAmerica" => "South America",
                null => string.Empty,
                _ => RegionName
            };
    }
}
