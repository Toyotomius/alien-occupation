﻿using Newtonsoft.Json;
using System;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary> Class containing all the global game variables. </summary>
    public class GlobalVariables
    {
        [JsonIgnore]
        public bool IsNewGame = false;

        public GlobalVariables()
        {
            Difficulty = new GameDifficulty();
        }

        public double BuildTimeModifier { get; set; } = 1D;
        public double BuildCostModifier { get; set; } = 1D;

        public DateTime Date { get; set; } = DateTime.Parse("2029/12/02 12:00:00"); //TODO: Remove default init. Temporary to avoid having to scene switch.

        public GameDifficulty Difficulty { get; set; }

        public class GameDifficulty
        {
            public float AttackModifier { get; set; } = 1F;

            public float FavorAdjustment { get; set; } = 1F;

            public float HappinessModifier { get; set; } = 1F;

            public bool IsRandomStartingStats { get; internal set; } = false; //TODO: Add new game start option screen.

            public int OverallDifficulty { get; }

            public float ProductivityModifier { get; set; } = 1F;

            public float QuotaRequirementModifier { get; set; } = 1F;

            public float ResourceRateModifier { get; set; } = 1F;

            public float RevoltRisk { get; set; } = 1F;

            public float RevoltSeverity { get; set; } = 1F;
        }
    }
}
