﻿namespace AlienOccupation.Assets.Resources.Global
{
    public interface ILoadVariableNames
    {
        void LoadDynamicResource();
    }
}