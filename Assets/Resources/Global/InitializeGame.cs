﻿using AlienOccupation.Assets.Resources.StrategicLayer.EventManager.InGameEvents;
using AlienOccupation.Assets.Resources.StrategicLayer.GameLogic.Revolts;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameEvents;
using AlienOccupation.Assets.Resources.StrategicLayer.InGameResources;
using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary>
    /// Initializes games with specific starting values and subscribes all required events. Triggers
    /// on both game load from GameStartMenu scene/menu and New Game. Checks for NewGame flag on
    /// initial load.
    /// </summary>
    public class InitializeGame : MonoBehaviour
    {
        private DateAndTime DateAndTime;
        private IGameEventHandler EventHandler;

        public void Start()
        {
            Constants.SetConstants();
            DateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();
            if (GameData.GDInstance.GlobalVariables.IsNewGame)
            {
                GameData.GDInstance.Regions = new StrategicLayer.WorldRegions(true);
                SetInitialValues();
            }

            //TODO: Move RegionDefaults back into newgame only
            GameData.GDInstance.Regions.SetRegionDefaults();

            EventHandler = Factory.CreateGameEventHandler();
            EventHandler.Load();

            SubscribeEvents();
            Factory.CreateLoadVariableNames().LoadDynamicResource();
        }

        private void SetInitialValues()
        {
            GameData.GDInstance.GlobalVariables.Date = Constants.StartDate;
        }

        private void SubscribeEvents()
        {
            //var montly = new MonthlyQuotaUpdate();
            //var dateTime = new DateAndTime();
            //dateTime.WeeklyChange += montly.UpdateQuota;

            // Notification subscriptions
            IPlayerNotification playerNotification = Factory.CreatePlayerNotification();

            EventHandler.NotifyPlayerOfEvent += playerNotification.RandomEventNotifyPlayer;

            // Resource updates
            var update = new ResourceUpdate();
            DateAndTime.DayChange += update.DailyUpdate;

            //dateAndTime.WeekChange += update.Update;
            //dateAndTime.MonthChange += update.Update;
            //dateAndTime.YearChange += update.Update;

            // Ingame event subscriptions - includes revolts
            IEventSelection daily = new DailyEventSelection();
            IEventSelection weekly = new WeeklyEventSelection();
            IEventSelection monthly = new MonthlyEventSelection();

            //IEventSelection yearly = new YearlyEventSelection();

            //dateAndTime.DayChange += daily.SelectEvent;
            //dateAndTime.WeekChange += weekly.SelectEvent;
            //dateAndTime.MonthChange += monthly.SelectEvent;
            //dateAndTime.YearChange += yearly.SelectEvent;

            var revoltCheck = new RevoltManager();
            DateAndTime.DayChange += revoltCheck.RevoltChecker;
            DateAndTime.WeekChange += revoltCheck.RevoltChecker;
            DateAndTime.MonthChange += revoltCheck.RevoltChecker;
            DateAndTime.YearChange += revoltCheck.RevoltChecker;
        }
    }
}
