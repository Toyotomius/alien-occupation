﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AlienOccupation.Assets.Resources.Global.Extensions
{
    public static class CreateInstanceExt
    {
        public static Dictionary<string, Type> CreateTypeDictionary(this Type type, Dictionary<string, Type> dict)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && p.IsClass).ToArray();
            foreach (var t in types)
            {
                
                dict.Add(t.Name, t);
            }
            return dict;
        }
    }
}
