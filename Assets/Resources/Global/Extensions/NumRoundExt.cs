﻿using System;

namespace AlienOccupation.Assets.Resources.Global.Extensions
{
    public static class NumRoundExt
    {
        public static int RoundToTen(this int i)
        {
            return (int)Math.Round(i / 10.0) * 10;
        }
        public static int RoundToTen(this double i)
        {
            return (int)Math.Round(i / 10.0) * 10;
        }
        public static float RoundToTen(this float f)
        {
            return (float)Math.Round(f, 1);
        }
    }
}
