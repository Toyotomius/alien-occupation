﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global.Extensions
{
    public static class NearbyTransformSearches
    {
        public static Transform DepthByName(this Transform transform, string name)
        {
            foreach (Transform t in transform)
            {
                if (t.name == name)
                {
                    return t.transform;
                }
                if (t.GetChild(0) != null)
                {
                    return t.GetChild(0).DepthByName(name);
                }
            }

            return null;
        }

        public static Transform ParentSiblingByName(this Transform transform, string name)
        {
            if (transform.parent != null)
            {
                foreach (Transform t in transform.parent)
                {
                    if (t.name == name)
                    {
                        return t.transform;
                    }
                }
                return transform.parent.ParentSiblingByName(name);
            }

            return null;
        }
    }
}
