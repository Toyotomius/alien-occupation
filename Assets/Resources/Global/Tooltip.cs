﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary>
    /// Singleton to cache the UI Tooltip box
    /// </summary>
    public class Tooltip
    {
        static Tooltip()
        {
        }

        private Tooltip()
        {
        }

        public static Tooltip Inst { get; } = new Tooltip();

        public GameObject UITooltip
        {
            get
            {
                // Check to adjust for switching game scenes. Will find and cache regardless of scene.
                if(tooltip == null)
                {
                    tooltip = GameObject.FindGameObjectWithTag("Canvas").transform.Find("UITooltipBackground").gameObject;
                }
                return tooltip;
            }
        }
        private GameObject tooltip;
    }
}
