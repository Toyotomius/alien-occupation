﻿using UnityEngine;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary>
    /// Quits game. May add autosave if Strategic Layer Scene is loaded.
    /// </summary>
    public class Quit : MonoBehaviour
    {
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
