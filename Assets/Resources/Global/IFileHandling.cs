﻿namespace AlienOccupation.Assets.Resources.Global
{
    public interface IFileHandling
    {
        void Load(string fileName);
        void Save(string saveName);
        void Delete(string fileName);
    }
}