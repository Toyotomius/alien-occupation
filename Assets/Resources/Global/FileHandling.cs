﻿using AlienOccupation.Assets.Resources.StrategicLayer.Buildings;
using AlienOccupation.Assets.Resources.StrategicLayer.GUIScripts;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AlienOccupation.Assets.Resources.Global
{
    /// <summary>
    /// Save game file handling class. JsonSerializerSettings required to avoid game load errors
    /// with certain classes.
    /// </summary>
    public class FileHandling : IFileHandling
    {
        public void Delete(string fileName)
        {
            File.Delete($@"{Application.persistentDataPath}\saves\{fileName}.sav");
        }

        public void Load(string fileName)
        {
            var json = "";

            using (StreamReader sr = new StreamReader($@"{Application.persistentDataPath}\saves\{fileName}.sav"))
            {
                json = sr.ReadToEnd();
            }
            
            GameData.GDInstance = JsonConvert.DeserializeObject<GameData>(json, new Newtonsoft.Json.JsonSerializerSettings
            {
                TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto
            });

            // Fixes bug when loading from StrategicLayer. Previous date fields retained state before
            // load, causing DateTime events to be raised.
            if (SceneManager.GetActiveScene().name == "StrategicLayer")
            {
                var dateAndTime = GameObject.Find("DateAndTime").GetComponent<DateAndTime>();
                dateAndTime.Start();
            }
            // Clears the researched buildings hashset. Bit of a hack but it'll do for now. Which means it's permanent.
            BuildingFactory.ClearResearchedBuildings();
            // Loads Resource properties that don't need to be saved in the save file (ex://sprite texture)
            GameData.GDInstance.PlayerResources.UpdateUnsavedProperties();
            // Applies researched tech effects
            foreach(var tech in GameData.GDInstance.Technology.Tech.Values)
            {
                if (tech.IsResearched)
                {
                    tech.ApplyEffects();
                }
            }
        }

        public void Save(string saveName)
        {
            var gameInstance = GameData.GDInstance;

            var json = JsonConvert.SerializeObject(gameInstance, Formatting.Indented, new Newtonsoft.Json.JsonSerializerSettings
            {
                TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto
            });
            using (StreamWriter sw = new StreamWriter($@"{Application.persistentDataPath}\saves\{saveName}.sav"))
            {
                sw.WriteLine(json);
            }
        }
    }
}
